//
// Copyright (c) 2015 Alexander Sacharov <a.sacharov@gmx.de>
//               All rights reserved.
//
// This work is licensed under the terms of the MIT license.
// For a copy, see <https://opensource.org/licenses/MIT>.
//

#ifndef F396C9F4_DCCF_483D_A25E_922B4B7F4E6B
#define F396C9F4_DCCF_483D_A25E_922B4B7F4E6B

//-----------------------------------------------------------------------------
// includes <...>
//-----------------------------------------------------------------------------

#ifdef __cplusplus
extern "C" {
#endif

//-----------------------------------------------------------------------------
// includes "..."
//-----------------------------------------------------------------------------
#include <stddef.h>
#include <stdint.h>

//----------------------------------------------------------------------------
// Defines and macros
//----------------------------------------------------------------------------
#define c_DEFAULT_STACK_SIZE  512

#define c_ThreadId            pthread_t
#define c_ThreadAttr          pthread_attr_t
#define c_ThreadDecl          void*
#define c_ThreadReturn        void*
#define c_ThreadArg           void*

#define c_CreateThread( pId, attr, pFunc, pFuncParams ) \
            ( !pthread_create( pId, attr, pFunc, (c_ThreadArg) pFuncParams ) )

#define c_ThreadStop( _h_ )                         c_StopThread( _h_ )
#define c_ThreadStart( _f_, _p_, _t_ )              c_StartThread( _f_, _p_, _t_ )
#define c_ThreadQuit( _h_ )                         c_QuitThread( _h_ )
#define c_ThreadInterval( _h_ )                     c_GetThreadInterval( _h_ )
#define c_ThreadSetMyInterval( _h_, _ms_ )          c_ChangeThisThreadInterval( _h_, _ms_ )
#define c_ThreadWakeUp( _h_ )                       c_WakeThread( _h_ )
#define c_ThreadSetInterval( _h_, _ms_ )            c_ChangeThreadInterval( _h_, _ms_ )

//----------------------------------------------------------------------------
// Typedefs, structs, enums, unions and variables
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// Public Prototypes
//----------------------------------------------------------------------------


/**
 * @brief :  typedef
 **/

typedef void * (* c_ThreadFunction )( void *threadParams );
typedef void * c_ThreadHandle;

c_ThreadHandle c_StartThread     ( c_ThreadFunction threadFunction,
                                   void *threadFunctionParams,
                                   unsigned long timeout );
/**
 * @brief : changed these three prototypes (and the routines), were *handle
 **/
void c_StopThread                  ( c_ThreadHandle handle );
/**
 * @brief : allow threads to terminate themselves
 **/
void c_QuitThread                  ( c_ThreadHandle handle );

unsigned long c_GetThreadInterval  ( c_ThreadHandle handle );
/**
 * @brief : allow threads to reschedule themselves
 **/
int  c_ChangeThisThreadInterval    ( c_ThreadHandle handle,
                                     unsigned long interval  );

/**
 * @brief : wakeup thread or change interval and wakeup thread
 **/
int  c_WakeThread                  ( c_ThreadHandle handle );
int  c_ChangeThreadInterval        ( c_ThreadHandle handle,
                                     unsigned long interval );

#ifdef __cplusplus
}
#endif

#endif /* F396C9F4_DCCF_483D_A25E_922B4B7F4E6B */









