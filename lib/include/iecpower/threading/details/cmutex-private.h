//
// Copyright (c) 2015 Alexander Sacharov <a.sacharov@gmx.de>
//               All rights reserved.
//
// This work is licensed under the terms of the MIT license.
// For a copy, see <https://opensource.org/licenses/MIT>.
//

#ifndef D54CC5F4_C9D8_4EBF_AE24_0371D2D89BDF
#define D54CC5F4_C9D8_4EBF_AE24_0371D2D89BDF

#include <stdint.h>
#include <pthread.h>

#ifdef __cplusplus
extern "C" {
#endif

//----------------------------------------------------------------------------
// Public defines and macros
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// Public typedefs, structs, enums, unions and variables
//----------------------------------------------------------------------------
/**
 * event structure
 */
typedef struct c_condition_s {
   pthread_mutex_t  mutex;
   pthread_cond_t   cond;
   int              oldval;
   int              newval;
} c_condition_t;

//----------------------------------------------------------------------------
// Public Function Prototypes
//----------------------------------------------------------------------------

/* Semaphore */
pthread_mutex_t* c_MutexCreate  ( pthread_mutex_t **mutex );
void c_MutexDelete              ( pthread_mutex_t **mutex );

/* Conditions */
c_condition_t* c_CondCreate     ( c_condition_t **cond );
void c_CondDelete               ( c_condition_t **cond );
int  c_CondWaitMs               ( c_condition_t *cond, unsigned long ms );
int  c_CondWaitUsec             ( c_condition_t *cond, unsigned long mks );
void c_CondSignal               ( c_condition_t *cond );
void c_CondSignalOneThread      ( c_condition_t *cond );
void c_CondReset                ( c_condition_t *cond );

#ifdef __cplusplus
}
#endif

#endif /* D54CC5F4_C9D8_4EBF_AE24_0371D2D89BDF */
