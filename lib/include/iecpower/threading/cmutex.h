//
// Copyright (c) 2015 Alexander Sacharov <a.sacharov@gmx.de>
//               All rights reserved.
//
// This work is licensed under the terms of the MIT license.
// For a copy, see <https://opensource.org/licenses/MIT>.
//

#ifndef AA5266E9_A521_4B0D_9582_7F4451C6A7D5
#define AA5266E9_A521_4B0D_9582_7F4451C6A7D5

#include "details/cmutex-private.h"   /* important defines */

#define c_Semaphore                    pthread_mutex_t *

#define c_CreateSem( _sem_ )           c_MutexCreate( &_sem_ )
#define c_DeleteSem( _sem_ )           c_MutexDelete( &_sem_ )
#define c_TakeSem( _sem_ )             pthread_mutex_lock( _sem_ )
#define c_TryTakeSem( _sem_ )          pthread_mutex_trylock( _sem_ )
#define c_ReleaseSem( _sem_ )          pthread_mutex_unlock( _sem_ )

#define c_Event                        c_condition_t *

#define c_CreateEvent( _cond_ )        c_CondCreate( &(_cond_) )
#define c_SignalEvent( _cond_ )        c_CondSignal( _cond_ )
#define c_SignalOneThread( _cond_ )    c_CondSignalOneThread( _cond_ )
#define c_ResetEvent( _cond_ )         c_CondReset( _cond_ )
#define c_WaitEvent( _cond_, ms )      c_CondWaitMs( _cond_, ms )
#define c_WaitEventMs( _cond_, ms )    c_CondWaitMs( _cond_, ms )
#define c_WaitEventMks( _cond_, usec ) c_CondWaitUsec( _cond_, usec )
#define c_DeleteEvent( _cond_ )        c_CondDelete( &_cond_ );

#endif /* AA5266E9_A521_4B0D_9582_7F4451C6A7D5 */
