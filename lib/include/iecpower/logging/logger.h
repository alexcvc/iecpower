//
// Copyright (c) 2015 Alexander Sacharov <a.sacharov@gmx.de>
//               All rights reserved.
//
// This work is licensed under the terms of the MIT license.
// For a copy, see <https://opensource.org/licenses/MIT>.
//

/*************************************************************************//**
 * \file        logger.h
 * \brief       contains logger multi-functions:
 *   - stderr;
 *   - server TCP for connect via tcp;
 *   - syslog UDP client;
 *
 * \author      Alexander Sacharov <as@asoft-labs.de>
 * \date        2021-07-26
 *****************************************************************************/

#ifndef B1943FFA_1AD1_4E0E_A752_75D5DCA539D5
#define B1943FFA_1AD1_4E0E_A752_75D5DCA539D5

#include <sys/types.h>
#include <syslog.h>
#include <assert.h>
#include <stdbool.h>
#include <stdint.h>

//----------------------------------------------------------------------------
// Public defines and macros
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// Public typedefs, structs, enums, unions and variables
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// Public Function Prototypes
//----------------------------------------------------------------------------
#ifdef __cplusplus
extern "C" {
#endif

/**
 *  call once with timestamp format "%y%m%d %H:%M:%S"
 */
bool logger_create                 ( const char *fmt );
void logger_closeall               ( void );
void logger_destroy                ( void );

/**
 *  Switch off/on or get status
 */
void logger_block                  ( void );
void logger_unblock                ( void );
int  logger_status                 ( void );

/**
 *  logging to syslog client
 */
int  logger_open_syslog            ( const char *logIp, uint16_t logPort,
                                     const char *ident, int options, int logfac );
void logger_close_syslog           ( void );
int  logger_setlogmask             ( int );

/**
 * logging with socket output. Return: 0 if successfully, otherwise -1
 */
int  logger_open_socket            ( uint16_t port, int level, int flags );
void logger_close_socket           ( void );

/**
 * debug file output. Return 0 if successfully, otherwise -1
 */
int  logger_open_file              ( const char *fname, int level, int flags );
void logger_trunc_file             ( off_t maxoff );
void logger_close_file             ( void );

/**
 * print to log
 */
void logger_print                  ( int priority, bool addnl, const char *fmt, ... );
void logger_with_strerr            ( int priority, bool addnl, const char *, ... );
void logger_quit                   ( int priority, bool addnl, const char *, ... );

void logger_hexdump                ( int priority, void* vpBuf, int length );
void logger_hexascii_dump          ( int priority, void *data, int size);

#ifdef __cplusplus
}
#endif

#endif /* B1943FFA_1AD1_4E0E_A752_75D5DCA539D5 */

