// Copyright(c) 2015 Gabi Melman.
// Distributed under the MIT License (http://opensource.org/licenses/MIT)
//
// Copyright(c) 2021 A.Sacharov.
// C++ wrapper class with  multiple loggers all sharing the same sink
//
// https://spdlog.docsforge.com/master/1.quickstart/#create-a-multiple-loggers-all-sharing-the-same-file-sink-aka-categories
// Docs Built May 30, 2021

/*************************************************************************/ /**
 * @file    writer.hh
 * \brief  C++ wrapper class with  multiple loggers all sharing the same sink) aka categories
 *
 * \author  AS
 * \date    2020-07-03
 *****************************************************************************/

#ifndef A117D01B_612A_41E2_BE3C_52569A58D56B
#define A117D01B_612A_41E2_BE3C_52569A58D56B

//-----------------------------------------------------------------------------
// includes <...>
//-----------------------------------------------------------------------------

#include <iostream>
#include <string>
#include <memory>
#include <vector>
#include <spdlog/sinks/basic_file_sink.h>
#include <spdlog/sinks/daily_file_sink.h>
#include <spdlog/sinks/rotating_file_sink.h>
#include <spdlog/sinks/stdout_sinks.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <spdlog/sinks/syslog_sink.h>
#include <spdlog/spdlog.h>

using std::shared_ptr;
using std::string;

//-----------------------------------------------------------------------------
// includes "..."
//-----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// Public defines and macros
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// Public typedefs, structs, enums, unions and variables
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// Public Function Prototypes
//----------------------------------------------------------------------------

namespace iecpower {
   /**
    * all in logging name environment
    */
   namespace log {

      /**
       * Kind of sink enumeration
       */
      enum class loggerKind {
         unknown,           ///< closed
         stdout_log,        ///< to stdout
         stderr_log,        ///< to stderr
         file_basic,        ///< to file
         file_daily,        ///< to file with daily append new file
         file_rotate,       ///< to rotaited limited file
         syslog,            ///< to  syslog
      };

      /**
       * Tuple with loggers as sharing to other file (sink) aka categories
       */
      struct logChannel{
         std::string name;
         std::shared_ptr<spdlog::logger>  log;
         spdlog::level::level_enum  level{spdlog::level::level_enum::off };
      };

      /**
       * @brief Class Log implements logger in  application
       *
       * Class can  support up to three multi-sinks in the logger
       */
      class writer {
      private:
         struct basis_writer{
            std::string name;
            std::shared_ptr<spdlog::logger>  log;
         };

         std::string m_name;                                ///< main logger name
         loggerKind m_logKind{loggerKind::unknown};         ///< kind of logging
         std::shared_ptr<spdlog::logger>  m_logPtr;         ///< main logger
         std::vector<logChannel> m_writers;                 ///< vector shared writers

      public:

         /// default constructor
         writer(const string &logger_name) : m_name(logger_name), m_logKind( loggerKind::unknown) {}

         /// destructor
         ~writer() { drop_all(); }

         /**
          * @brief is valid
          * @return true if logger was created, otherwise false
          */
         bool is_valid() {
            if(m_logPtr) {
               return true;
            }
            return m_logPtr ? true : false;
         }

         /**
          * @brief gets log name
          * @return name
          */
         const string &getName() const {
            return m_name;
         }

         /**
          * gets current log kind
          * @return kind of log
          */
         loggerKind getSinkKind() const {
            return m_logKind;
         }

         /**
          * gets shared pointer to log
          * @return shared pointer
          */
         const shared_ptr<spdlog::logger> &logPtr() const {
            return m_logPtr;
         }

         const std::vector<logChannel> &writers() const {
            return m_writers;
         }

         /**
          * @brief has console logging
          * @return true if console logger was created, otherwise false
          */
         bool has_stdout_log() {
            return m_logKind == loggerKind::stdout_log ? true : false;
         }

         /**
          * @brief has console colored logging
          * @return true if console logger was created, otherwise false
          */
         bool has_stderr_log() {
            return m_logKind == loggerKind::stderr_log ? true : false;
         }

         /**
          * @brief has file logging
          * @return true if file logger was created, otherwise false
          */
         bool has_file_basic_log() {
            return m_logKind == loggerKind::file_basic ? true : false;
         }

         /**
          * @brief has file daily logging
          * @return true if file logger was created, otherwise false
          */
         bool has_file_daily_log() {
            return m_logKind == loggerKind::file_daily ? true : false;
         }

         /**
          * @brief has file rotaited logging
          * @return true if file logger was created, otherwise false
          */
         bool has_file_rotated_log() {
            return m_logKind == loggerKind::file_rotate ? true : false;
         }

         /**
          * @brief has syslog logging
          * @return true if syslog logger was created, otherwise false
          */
         bool has_syslog() {
            return m_logKind == loggerKind::syslog ? true : false;
         }

         /**
          * @brief get level
          */
         spdlog::level::level_enum level() {
            if (m_logPtr) return m_logPtr->level();
            else return spdlog::level::off;
         }

         /**
          * sets log level vor base logging
          * @param lev
          */
         void set_level(spdlog::level::level_enum lev = spdlog::level::debug) {
            if (m_logPtr) return m_logPtr->set_level(lev);
         }

         /**
          * @brief set logging level
          * @param flag - logging part
          * @param lev - level
          */
         spdlog::level::level_enum level_writer( const std::string& name ) {
            for (auto &item : m_writers) {
               if (name.compare(name) == 0) {
                  if( item.log ) {
                     return item.log->level();
                  }
                  return spdlog::level::level_enum::off;
               }
            }
         }

         /**
          * @brief set logging level
          * @param flag - logging part
          * @param lev - level
          */
         void set_level_writer(const std::string& name, spdlog::level::level_enum lev) {
            for (auto &item : m_writers) {
               if (name.compare(name) == 0) {
                  if( item.log ) {
                     item.log->set_level(lev);
                  }
                  break;
               }
            }
         }

         /**
          * @brief set level for all  flags
          * @param lev
          */
         void set_level_all(spdlog::level::level_enum lev) {
            if (m_logPtr) m_logPtr->set_level(lev);
            for (auto &item : m_writers) {
               if (item.log) {
                  item.log->set_level(lev);
               }
            }
         }

         /**
          * @brief open a stdout logger
          * @param logger name
          * @return true if successfully, otherwise - false
          */
         bool open_stdout_log(spdlog::level::level_enum lev = spdlog::level::warn) {
            try {
               // clear kind
               m_logKind = loggerKind::unknown;
               // close all
               reset_writers();

               // drop base
               if (m_logPtr) {
                  m_logPtr->flush();
                  spdlog::drop(m_name);
                  m_logPtr.reset();
               }

               m_logPtr = spdlog::stdout_color_mt(m_name);
               m_logPtr->set_level(lev);

               // create writers
               for (auto &item : m_writers) {
                  item.log = spdlog::stdout_color_mt(item.name);
                  item.log->set_level(item.level);
               }
               // set kind
               m_logKind = loggerKind::stdout_log;
               return true;
            }
            catch (const spdlog::spdlog_ex &ex) {
               std::cerr << "stdout log initialization failed: " << ex.what() << std::endl;
               drop_all();
               return false;
            }
         }

         /**
          * add new writer
          * @param name
          * @param lev
          * @return
          */
         bool add_stdout_writer(const std::string& name, spdlog::level::level_enum lev = spdlog::level::warn) {
            try {
               // drop - may be exist!
               drop_writer(name);

               // new writer
               logChannel new_writer;

               new_writer.name = name;
               new_writer.log = spdlog::stdout_color_mt(new_writer.name);
               if(!new_writer.log) throw spdlog::spdlog_ex("writer log create failed");
               new_writer.level = lev;
               new_writer.log->set_level(new_writer.level);

               m_writers.push_back(new_writer);
               return true;
            }
            catch (const spdlog::spdlog_ex &ex) {
               std::cerr << "add stdout writer failed: " << ex.what() << std::endl;
               drop_writer(name);
               return false;
            }
         }

         /**
          * @brief open a stdout logger
          * @param logger name
          * @return true if successfully, otherwise - false
          */
         bool open_stderr_log(spdlog::level::level_enum lev = spdlog::level::warn) {
            try {
               // clear kind
               m_logKind = loggerKind::unknown;
               // close all
               reset_writers();

               // drop base
               if (m_logPtr) {
                  m_logPtr->flush();
                  spdlog::drop(m_name);
                  m_logPtr.reset();
               }

               m_logPtr = spdlog::stderr_color_mt(m_name);
               m_logPtr->set_level(lev);

               // create writers
               for (auto &item : m_writers) {
                  item.log = spdlog::stderr_color_mt(item.name);
                  item.log->set_level(item.level);
               }
               // set kind
               m_logKind = loggerKind::stderr_log;
               return true;
            }
            catch (const spdlog::spdlog_ex &ex) {
               std::cerr << "stderr log initialization failed: " << ex.what() << std::endl;
               drop_all();
               return false;
            }
         }

         /**
          * add new writer
          * @param name
          * @param lev
          * @return
          */
         bool add_stderr_writer(const std::string& name, spdlog::level::level_enum lev = spdlog::level::warn) {
            try {
               // drop - may be exist!
               drop_writer(name);

               // new writer
               logChannel new_writer;

               new_writer.name = name;
               new_writer.log = spdlog::stderr_color_mt(new_writer.name);
               if(!new_writer.log) throw spdlog::spdlog_ex("writer log create failed");
               new_writer.level = lev;

               m_writers.push_back(new_writer);
               return true;
            }
            catch (const spdlog::spdlog_ex &ex) {
               std::cerr << "add stdout writer failed: " << ex.what() << std::endl;
               drop_writer(name);
               return false;
            }
         }

         /**
          * @brief open a file rotating logger with kb size max and x rotated files
          * @param fileName
          * @param maxSizeFileKb - the max file size
          * @param numbRotationFiles - the max number of files
          * @return true if successfully, otherwise - false
          */
         bool open_rotation_file_log(const string &fileName, size_t maxSizeFileKb, size_t numbRotationFiles,
                                     spdlog::level::level_enum lev = spdlog::level::info) {
            try {
               // clear kind
               m_logKind = loggerKind::unknown;
               // close all
               reset_writers();

               // drop base
               if (m_logPtr) {
                  m_logPtr->flush();
                  spdlog::drop(m_name);
                  m_logPtr.reset();
               }

               // create base
               m_logPtr = spdlog::rotating_logger_mt(m_name, fileName, maxSizeFileKb, numbRotationFiles);
               if(!m_logPtr) throw spdlog::spdlog_ex("base log create failed");
               m_logPtr->set_level(lev);

               // create sinks
               for (auto &item : m_writers) {
                  auto rotating = std::make_shared<spdlog::sinks::rotating_file_sink_mt> ( fileName, maxSizeFileKb, numbRotationFiles);
                  if(!rotating) throw spdlog::spdlog_ex("writer sink create failed");;
                  // add to log
                  m_logPtr->sinks().push_back(rotating);
                  // find log
                  item.log = spdlog::get(item.name);
                  if(!item.log) throw spdlog::spdlog_ex("writer log get failed");;
                  item.log->set_level(lev);
               }
               // set kind
               m_logKind = loggerKind::file_rotate;
               return true;
            }
            catch (const spdlog::spdlog_ex &ex) {
               std::cerr << "stdout log initialization failed: " << ex.what() << std::endl;
               drop_all();
               return false;
            }
         }

         /**
          * @brief open logger which sends its logging to syslog.
          * @param logger_name
          * @param syslog_ident
          * @param syslog_option
          * @param syslog_facility
          * @param enable_formatting
          */
         bool open_syslog_log(const std::string &syslog_ident, int syslog_option = 0,
                              int syslog_facility = LOG_USER, bool enable_formatting = false,
                              spdlog::level::level_enum lev = spdlog::level::info) {
            try {
               // clear kind
               m_logKind = loggerKind::unknown;
               // close all
               reset_writers();

               // drop base
               if (m_logPtr) {
                  m_logPtr->flush();
                  spdlog::drop(m_name);
                  m_logPtr.reset();
               }

               // create base
               m_logPtr = spdlog::syslog_logger_mt(m_name, syslog_ident, syslog_option, syslog_facility, enable_formatting);
               if(!m_logPtr) throw spdlog::spdlog_ex("syslog create failed");
               m_logPtr->set_level(lev);

               // create sinks
               for (auto &item : m_writers) {
                  auto rotating = std::make_shared<spdlog::sinks::syslog_sink_mt> (syslog_ident, syslog_option, syslog_facility, enable_formatting);
                  if(!rotating) throw spdlog::spdlog_ex("syslog sink create failed");;
                  // add to log
                  m_logPtr->sinks().push_back(rotating);
                  // find log
                  item.log = spdlog::get(item.name);
                  if(!item.log) throw spdlog::spdlog_ex("syslog log get failed");;
                  item.log->set_level(lev);
               }
               // set kind
               m_logKind = loggerKind::file_rotate;
               return true;
            }
            catch (const spdlog::spdlog_ex &ex) {
               std::cerr << "syslog log initialization failed: " << ex.what() << std::endl;
               drop_all();
               return false;
            }
         }

         /// close logging
         /// \return
         void drop_writer(const std::string &name) {
            for (std::vector<log::logChannel>::iterator it = m_writers.begin(); it != m_writers.end(); ++it) {
               if (it->name.compare(name) == 0) {
                  if (it->log) it->log.reset();
                  spdlog::drop(it->name);
                  m_writers.erase(it);
                  break;
               }
            }
         }

         /// close logging
         /// \return
         void erase_writer(const std::string &name) {
            drop_writer(name);
         }

         /// close logging
         /// \return
         void reset_writers() {
            for (auto &item : m_writers) {
               if (item.log) item.log.reset();
               spdlog::drop(item.name);
            }
         }

         /// drop base logging with all writers
         /// \return
         void drop_all() {
            // drop writers
            for (auto &item : m_writers) {
               spdlog::drop(item.name);
            }
            // clear vector writers
            m_writers.clear();
            // drop logging
            spdlog::drop(m_name);
            if (m_logPtr) m_logPtr.reset();
         }

         //----------------------------------------------------------
         // trace
         //----------------------------------------------------------
         template<typename... Args>
         inline void trace(fmt::basic_string_view<char> fmt, const Args &...args) {
            if (is_valid()) {
               m_logPtr->trace(fmt, args...);
            }
         }

         template<typename T>
         inline void trace(const T &msg) {
            if (is_valid()) {
               // logging to multi-sink logger
               m_logPtr->trace(msg);
            }
         }

         template<typename... Args>
         inline void trace_if(const bool flag, fmt::basic_string_view<char> fmt, const Args &...args) {
            if (flag && is_valid()) {
               m_logPtr->trace(fmt, args...);
            }
         }

         template<typename T>
         inline void trace_if(const bool flag, const T &msg) {
            if (flag && is_valid()) {
               // logging to multi-sink logger
               m_logPtr->trace(msg);
            }
         }

         template<typename... Args>
         inline void trace_inst(const std::string &name, fmt::basic_string_view<char> fmt, const Args &...args) {
            std::shared_ptr<spdlog::logger> logPtr = spdlog::get(name);
            if (logPtr && (logPtr->level() >= spdlog::level::trace)) {
               // logging to multi-sink logger
               logPtr->trace(fmt, args...);
            }
         }

         template<typename T>
         inline void trace_inst(const std::string &name, const T &msg) {
            std::shared_ptr<spdlog::logger> logPtr = spdlog::get(name);
            if (logPtr && (logPtr->level() >= spdlog::level::trace)) {
               // logging to multi-sink logger
               logPtr->trace(msg);
            }
         }

         //----------------------------------------------------------
         // debug
         //----------------------------------------------------------
         template<typename... Args>
         inline void debug(fmt::basic_string_view<char> fmt, const Args &...args) {
            if (is_valid()) {
               m_logPtr->debug(fmt, args...);
            }
         }

         template<typename T>
         inline void debug(const T &msg) {
            if (is_valid() ) {
               // logging to multi-sink logger
               m_logPtr->debug(msg);
            }
         }

         template<typename... Args>
         inline void debug_if(const bool flag, fmt::basic_string_view<char> fmt, const Args &...args) {
            if (flag &&  is_valid()) {
               m_logPtr->debug(fmt, args...);
            }
         }

         template<typename T>
         inline void debug_if(const bool flag, const T &msg) {
            if (flag && is_valid() ) {
               // logging to multi-sink logger
               m_logPtr->debug(msg);
            }
         }

         template<typename... Args>
         inline void debug_inst(const std::string &name, fmt::basic_string_view<char> fmt, const Args &...args) {
            std::shared_ptr<spdlog::logger> logPtr = spdlog::get(name);
            if (logPtr && (logPtr->level() >= spdlog::level::debug)) {
               // logging to multi-sink logger
               logPtr->debug(fmt, args...);
            }
         }

         template<typename T>
         inline void debug_inst(const std::string &name, const T &msg) {
            std::shared_ptr<spdlog::logger> logPtr = spdlog::get(name);
            if (logPtr && (logPtr->level() >= spdlog::level::debug)) {
               // logging to multi-sink logger
               logPtr->debug(msg);
            }
         }

         //----------------------------------------------------------
         // info
         //----------------------------------------------------------
         template<typename... Args>
         inline void info(fmt::basic_string_view<char> fmt, const Args &...args) {
            if (is_valid()) {
               m_logPtr->info(fmt, args...);
            }
         }

         template<typename T>
         inline void info(const T &msg) {
            if (is_valid()) {
               // logging to multi-sink logger
               m_logPtr->info(msg);
            }
         }

         template<typename... Args>
         inline void info_if(const bool flag, fmt::basic_string_view<char> fmt, const Args &...args) {
            if (flag && is_valid()) {
               m_logPtr->info(fmt, args...);
            }
         }

         template<typename T>
         inline void info_if(const bool flag, const T &msg) {
            if (flag && is_valid()) {
               // logging to multi-sink logger
               m_logPtr->info(msg);
            }
         }

         template<typename... Args>
         inline void info_inst(const std::string &name, fmt::basic_string_view<char> fmt, const Args &...args) {
            std::shared_ptr<spdlog::logger> logPtr = spdlog::get(name);
            if (logPtr && (logPtr->level() >= spdlog::level::info)) {
               // logging to multi-sink logger
               logPtr->info(fmt, args...);
            }
         }

         template<typename T>
         inline void info_inst(const std::string &name, const T &msg) {
            std::shared_ptr<spdlog::logger> logPtr = spdlog::get(name);
            if (logPtr && (logPtr->level() >= spdlog::level::info)) {
               // logging to multi-sink logger
               logPtr->info(msg);
            }
         }

         //----------------------------------------------------------
         // warn
         //----------------------------------------------------------
         template<typename... Args>
         inline void warn(fmt::basic_string_view<char> fmt, const Args &...args) {
            if (is_valid()) {
               m_logPtr->warn(fmt, args...);
            }
         }

         template<typename T>
         inline void warn(const T &msg) {
            if (is_valid() ) {
               // logging to multi-sink logger
               m_logPtr->warn(msg);
            }
         }

         template<typename... Args>
         inline void warn_if(const bool flag, fmt::basic_string_view<char> fmt, const Args &...args) {
            if (flag && is_valid()) {
               m_logPtr->warn(fmt, args...);
            }
         }

         template<typename T>
         inline void warn_if(const bool flag, const T &msg) {
            if (flag && is_valid() ) {
               // logging to multi-sink logger
               m_logPtr->warn(msg);
            }
         }

         template<typename... Args>
         inline void warn_inst(const std::string &name, fmt::basic_string_view<char> fmt, const Args &...args) {
            std::shared_ptr<spdlog::logger> logPtr = spdlog::get(name);
            if (logPtr && (logPtr->level() >= spdlog::level::warn)) {
               // logging to multi-sink logger
               logPtr->warn(fmt, args...);
            }
         }

         template<typename T>
         inline void warn_inst(const std::string &name, const T &msg) {
            std::shared_ptr<spdlog::logger> logPtr = spdlog::get(name);
            if (logPtr && (logPtr->level() >= spdlog::level::warn)) {
               // logging to multi-sink logger
               logPtr->warn(msg);
            }
         }

         //----------------------------------------------------------
         // error
         //----------------------------------------------------------
         template<typename... Args>
         inline void error(fmt::basic_string_view<char> fmt, const Args &...args) {
            if (is_valid()) {
               m_logPtr->error(fmt, args...);
            }
         }

         template<typename T>
         inline void error(const T &msg) {
            if (is_valid() ) {
               // logging to multi-sink logger
               m_logPtr->error(msg);
            }
         }

         template<typename... Args>
         inline void error_if(const bool flag, fmt::basic_string_view<char> fmt, const Args &...args) {
            if (flag && is_valid()) {
               m_logPtr->error(fmt, args...);
            }
         }

         template<typename T>
         inline void error_if(const bool flag, const T &msg) {
            if (flag && is_valid() ) {
               // logging to multi-sink logger
               m_logPtr->error(msg);
            }
         }

         template<typename... Args>
         inline void error_inst(const std::string &name, fmt::basic_string_view<char> fmt, const Args &...args) {
            std::shared_ptr<spdlog::logger> logPtr = spdlog::get(name);
            if (logPtr) {
               // logging to multi-sink logger
               logPtr->error(fmt, args...);
            }
         }

         template<typename T>
         inline void error_inst(const std::string &name, const T &msg) {
            std::shared_ptr<spdlog::logger> logPtr = spdlog::get(name);
            if (logPtr) {
               // logging to multi-sink logger
               logPtr->error(msg);
            }
         }

         //----------------------------------------------------------
         // critical
         //----------------------------------------------------------
         template<typename... Args>
         inline void critical(fmt::basic_string_view<char> fmt, const Args &...args) {
            if (is_valid()) {
               m_logPtr->critical(fmt, args...);
            }
         }

         template<typename T>
         inline void critical(const T &msg) {
            if (is_valid() ) {
               // logging to multi-sink logger
               m_logPtr->critical(msg);
            }
         }

         template<typename... Args>
         inline void critical_if(const bool flag, fmt::basic_string_view<char> fmt, const Args &...args) {
            if (flag && is_valid()) {
               m_logPtr->critical(fmt, args...);
            }
         }

         template<typename T>
         inline void critical_if(const bool flag, const T &msg) {
            if (flag && is_valid() ) {
               // logging to multi-sink logger
               m_logPtr->critical(msg);
            }
         }

         template<typename... Args>
         inline void critical_inst(const std::string &name, fmt::basic_string_view<char> fmt, const Args &...args) {
            std::shared_ptr<spdlog::logger> logPtr = spdlog::get(name);
            if (logPtr) {
               // logging to multi-sink logger
               logPtr->critical(fmt, args...);
            }
         }

         template<typename T>
         inline void critical_inst(const std::string &name, const T &msg) {
            std::shared_ptr<spdlog::logger> logPtr = spdlog::get(name);
            if (logPtr) {
               // logging to multi-sink logger
               logPtr->critical(msg);
            }
         }

      };
   } // end of logging
} // end of iecpower

typedef std::shared_ptr<iecpower::log::writer> log4cppPtr;

#endif /* A117D01B_612A_41E2_BE3C_52569A58D56B */
