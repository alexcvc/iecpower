//
// Copyright (c) 2020 Alexander Sacharov and project contributors.
//               All rights reserved.
//
// This work is licensed under the terms of the MIT license.
// For a copy, see <https://opensource.org/licenses/MIT>.
//

#ifndef C2F67C01_5AC5_42AA_A443_2EB4B01BEA97
#define C2F67C01_5AC5_42AA_A443_2EB4B01BEA97

#include <syslog.h>
#include <stdio.h>
#include <stdint.h>
#include <stdarg.h>
#include <unistd.h>

#ifdef __cplusplus
extern "C" {
#endif

int logclient_closelog      ( void );
/**************************************************************************//**
 * @brief open syslog to remote host
 * @param IP - host
 * @param port on remote host
 * @param ident name
 * @param options
 *   LOG_PID     0x01  log the pid with each message
 *   LOG_CONS    0x02  log on the console if errors in sending
 *   LOG_ODELAY  0x04  delay open until first syslog() (default)
 *   LOG_NDELAY  0x08  don't delay open
 *   LOG_PERROR  0x20  log to stderr as well
 * @param facilities
 *   LOG_KERN	   (0<<3)	 kernel messages
 *   LOG_USER	   (1<<3)	 random user-level messages
 *   LOG_MAIL	   (2<<3)	 mail system
 *   LOG_DAEMON   (3<<3)	 system daemons
 *   LOG_AUTH	   (4<<3)	 security/authorization messages
 *   LOG_SYSLOG   (5<<3)	 messages generated internally by syslogd
 *   LOG_LPR	   (6<<3)	 line printer subsystem
 *   LOG_NEWS	   (7<<3)	 network news subsystem
 *   LOG_UUCP	   (8<<3)	 UUCP subsystem
 *   LOG_CRON	   (9<<3)	 clock daemon
 *   LOG_AUTHPRIV	(10<<3)	 security/authorization messages (private)
 *   LOG_FTP		(11<<3)	 ftp daemon
 *
 *   	 other codes through 15 reserved for system use
 *   LOG_LOCAL0	(16<<3)	 reserved for local use
 *   LOG_LOCAL1	(17<<3)	 reserved for local use
 *   LOG_LOCAL2	(18<<3)	 reserved for local use
 *   LOG_LOCAL3	(19<<3)	 reserved for local use
 *   LOG_LOCAL4	(20<<3)	 reserved for local use
 *   LOG_LOCAL5	(21<<3)	 reserved for local use
 *   LOG_LOCAL6	(22<<3)	 reserved for local use
 *   LOG_LOCAL7	(23<<3)	 reserved for local use
 *
 *   Example
 *   extern char       *__progname;
 *   logclient_openlog( "192.168.1.2", 514, __progname, LOG_PID, LOG_USER );
 *
 * ***************************************************************************/
int logclient_openlog       ( const char *logIp, uint16_t logPort, const char *ident, int options, int logfac);

/**
 * priorities/facilities are encoded into a single 32-bit quantity, where the
 * bottom 3 bits are the priority (0-7) and the top 28 bits are the facility
 * (0-big number).  Both the priorities and the facilities map roughly
 * one-to-one to strings in the syslogd(8) source code.  This mapping is
 * included in this file.
 *
 * priorities (these are ordered)
 *  LOG_EMERG   0   system is unusable
 *  LOG_ALERT   1   action must be taken immediately
 *  LOG_CRIT    2   critical conditions
 *  LOG_ERR     3   error conditions
 *  LOG_WARNING 4   warning conditions
 *  LOG_NOTICE  5   normal but significant condition
 *  LOG_INFO    6   informational
 *  LOG_DEBUG   7   debug-level messages
 */
int logclient_setlogmask   ( int );
int logclient_syslog       ( int, const char *, ... );
int logclient_vsyslog      ( int pri, const char *fmt, va_list ap );

const char *syslog_strerror( void );

#ifdef __cplusplus
};
#endif

#endif /* C2F67C01_5AC5_42AA_A443_2EB4B01BEA97 */
