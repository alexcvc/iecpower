//
// Copyright (c) 2020 Alexander Sacharov and project contributors.
//               All rights reserved.
//
// This work is licensed under the terms of the MIT license.
// For a copy, see <https://opensource.org/licenses/MIT>.
//

/**
 * Handling
 * @created : 2010-06-05
 * @changes :
 *
 * This file contains the following routines:
 *
 */
#ifndef A65DBC3F_759D_4FB4_AEFB_EAD9D851CF29
#define A65DBC3F_759D_4FB4_AEFB_EAD9D851CF29

#define LOGGING_USE_DEFAULT_LEVEL       (-1)
#define LOGGING_DEFAULT_FMT_TIMESTAMP   "[%F %T] "

/**
 * @brief : function prototypes
 **/

#ifdef __cplusplus
extern "C" {
#endif

/**
 * initialize tklog
 **/
int logsrv_init(void);

/* set default log level */
int logsrv_set_default_level(int lvl);

/* set timestamp format (strftime() format) */
void logsrv_set_timestamp_format(const char *fmt);

/* log to fd if message log level is less or equal lvl */
int logsrv_fd_start( int fd,
                    int lvl,
                    int flags);

/* stop logging to fd */
int logsrv_fd_stop(int fd);

/**
 * log to socket
 **/

/* log to socket if message log level is less or equal lvl */
int logsrv_socket_start(int port, int lvl, int flags);

/* stop logging on socket */
int logsrv_socket_stop(int port);

/**
 * log via syslog
 **/
int logsrv_syslog_start(const char *ident,
                        int option,
                        int facility );

/* stop logging via syslog */
int logsrv_syslog_stop(void);

/* set TKLog level syslog prio mapping */
void logsrv_syslog_set_prio_map(int *map, int len);

/**
 * misc
 **/

/* log a string */
int logsrv_puts(int lvl, const char *buffer);

/* log a formatted message */
int logsrv_printf(int lvl, const char *fmt, ...);

#ifdef __cplusplus
}
#endif

/* logsrv_..._start() flags */
#define LOGGING_FL_NOTIME   ( 1<<0 )     /* no timestamp */
#define LOGGING_FL_CLOSE    ( 1<<1 )     /* close fd on stop */

#endif /* A65DBC3F_759D_4FB4_AEFB_EAD9D851CF29 */
