//
// Copyright (c) 2015 Alexander Sacharov <a.sacharov@gmx.de>
//               All rights reserved.
//
// This work is licensed under the terms of the MIT license.
// For a copy, see <https://opensource.org/licenses/MIT>.
//

/*************************************************************************//**
 * \file        arp_sendto.hh
 * \brief       send arp request.
 * \author      Alexander Sacharov <as@asoft-labs.de>
 * \date        2021-07-26
 *****************************************************************************/

//-----------------------------------------------------------------------------
// includes <...>
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// includes "..."
//-----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// Public defines and macros
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// Public typedefs, structs, enums, unions and variables
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// Public Function Prototypes
//----------------------------------------------------------------------------

#ifndef E2F67333_3316_4797_AEB3_35530E1E13BC
#define E2F67333_3316_4797_AEB3_35530E1E13BC

/*************************************************************************//**
 * Send IPv4 ARP Packet.
 * Send an IPv4 ARP packet via raw socket at the link layer (ethernet frame).
 * Values set for ARP request.
 * @param interface
 * @param senderIp
 * @param targetIp
 * @param waitRspMs
 * @param verbose
 * @param stream
 * @return  0 if successfully, otherwise -1 if an error occurred
 */
int send_arp( const char *interface,
                const char *senderIp, const char *targetIp,
                uint32_t waitRspMs,
                uint8_t verbose, FILE *stream );

#endif /* E2F67333_3316_4797_AEB3_35530E1E13BC */

