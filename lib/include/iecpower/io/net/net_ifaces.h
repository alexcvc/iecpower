/****************************************************************************\
**                   _        _____ _               _                      **
**                  / \      | ____| |__   ___ _ __| | ___                 **
**                 / _ \     |  _| | '_ \ / _ \ '__| |/ _ \                **
**                / ___ \ _  | |___| |_) |  __/ |  | |  __/                **
**               /_/   \_(_) |_____|_.__/ \___|_|  |_|\___|                **
**                                                                         **
*****************************************************************************
** Copyright (c) 2010 - 2021 A. Eberle GmbH & Co. KG. All rights reserved. **
\****************************************************************************/

/*************************************************************************//**
* @file    net_ifaces.hh
* @brief   net adapter class
* @author  Alexander Sacharov <a.sacharov@asoft-labs.de>
* @ingroup COM4CPP Library
*****************************************************************************/


#ifndef D36BD06A_DC10_41A1_9CCF_A78DB32B30BD
#define D36BD06A_DC10_41A1_9CCF_A78DB32B30BD

#include <stdbool.h>
#include <netdb.h>
#include <ifaddrs.h>
#include <net/if.h>
#include <net/ethernet.h>

#define IFMAXNUM   32

/* local interface info */
typedef struct net_iface_t {
   char              name[IFNAMSIZ];
   struct ether_addr mac;
   struct in_addr    ip;
   struct in_addr    bc;
   struct in_addr    nm;
   u_short           mtu;
} net_iface_t;

/* local interface info */
typedef struct net_iface_list_t {
   int              number;
   net_iface_t      ifc[IFMAXNUM];
} net_iface_list_t;

/*************************************************************************//**
 * returns list interfaces
 * @param iflist
 * @param verbose
 * @param outbuf
 * @param len
 * @return 0 if successfully, otherwise - -1
 */
int net_get_ifaces       ( net_iface_list_t *iflist, uint8_t verbose, char *outbuf, size_t len );

/*************************************************************************//**
 * reads mac-address
 * @param name
 * @param mac
 * @return 0 if successfully, otherwise - -1
 */
int net_get_mac_address  ( const char *name, struct ether_addr *mac );

/*************************************************************************//**
 * @brief find local IP/netmask by remote server host
 * @param ipHost - remote server
 * @param localIp
 * @param localNetmask
 * @param ethernet name
 * @param ethernet name length
 * @return 0 if successfully, otherwise - -1
 */
int net_resolve_local_interface( const uint32_t remoteHost, uint32_t* localIp, uint32_t* localNetmask, char *ethName, int lenEthName );

/*************************************************************************//**
 * @brief   gets IP-address of name on network order .
 * @details returns IP-address of name on network order .
 * @return  Return IP-address of name on network order .
 */
uint32_t net_resolve_ip_network_order (const char *name);

/*************************************************************************//**
 * @brief Check if host address 'ip' replies on ARP
 * @param ip - remote IP
 * @return 0 if successfully, otherwise -1 if an error occurred
 */
int net_check_arp_reply_from_host (const  char *ethName, uint32_t ip, uint32_t ipLan, uint32_t netmaskLan);

/*************************************************************************//**
 * @brief Return true if 'ip' is on this LAN..
 * @param IP
 * @param lan IP
 * @param lan netmask
 * @return true if lan has the host address, otherwise - false
 */
__attribute__ ((unused))
static bool net_has_lan_host_address (uint32_t ipHost, uint32_t ipLan, uint32_t netmaskLan )
{
   return (((ntohl( ipHost ) ^ ntohl( ipLan )) & ntohl( netmaskLan )) == 0);
}

/*************************************************************************//**
 * @brief   checks if 'ip' is a (directed) ip-broadcast address.
 * @param IP
 * @param lan netmask
 * @return Return true if 'ip' is a (directed) ip-broadcast address
 */
__attribute__ ((unused))
static bool net_is_broadcast (uint32_t ip, uint32_t netmask )
{
   return ((~ntohl( ip ) & ~ntohl( netmask )) == 0);
}

#endif /* D36BD06A_DC10_41A1_9CCF_A78DB32B30BD */
