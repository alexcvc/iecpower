//
// Copyright (c) 2015 Alexander Sacharov <a.sacharov@gmx.de>
//               All rights reserved.
//
// This work is licensed under the terms of the MIT license.
// For a copy, see <https://opensource.org/licenses/MIT>.
//

/*************************************************************************//**
 * \file        cthread.c
 * \brief       This contains functions to start, stop, and wakeup
 * a generalized thread.  A function which implements the main execution 
 * code of thread is passed in at startup.
 * \author      Alexander Sacharov <as@asoft-labs.de> 
 *****************************************************************************/

//-----------------------------------------------------------------------------
// includes <...>
//-----------------------------------------------------------------------------
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <unistd.h>
#include <assert.h>
#include <errno.h>
#include <pthread.h>
#include <malloc.h>
#include <limits.h>

//-----------------------------------------------------------------------------
// includes "..."
//-----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// Defines and Macros
//----------------------------------------------------------------------------
#include "iecpower/threading/cmutex.h"   
#include "iecpower/threading/cthread.h"  

//----------------------------------------------------------------------------
// Typedefs, enums, unions, variables
//----------------------------------------------------------------------------
typedef enum {
   THREAD_IDLE,
   THREAD_RUNNING,
   THREAD_STOPPING,
   THREAD_STOPPED,
   THREAD_QUITTING          /* allow threads to quit */
} c_ThreadState;

typedef struct c_ThreadParams {
   c_ThreadState     State;
   c_Event           WakeUpEvent;
   unsigned long       Timeout;
   c_ThreadFunction  ThreadFunction;
   void                *ThreadFunctionParams;
   c_ThreadId        ThreadId;
} c_ThreadParams;

//----------------------------------------------------------------------------
// Functions Prototypes
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// Function Definitions
//----------------------------------------------------------------------------

c_ThreadDecl   c_Thread      ( c_ThreadArg pParams );

/** ================================================================================================
 * @brief    : create thread
 * @argumets :
 * @return   :
 **/
int c_CreateStackThread(c_ThreadId *pId, c_ThreadFunction pFunc, c_ThreadArg pFuncParams) {
   if (pthread_create(pId, NULL, pFunc, pFuncParams)) {
      return (0);
   }

   if (pthread_detach(*pId))
      return (0);

   usleep(1);
   return (1);
}

/** ================================================================================================
 * @brief    :  start thread
 * @note     :
 * @argumets :
 * @return   :
 **/
c_ThreadHandle c_StartThread ( c_ThreadFunction threadFunction,
                                   void *threadFunctionParams,
                                   unsigned long timeout )
{
    c_ThreadParams *params;

    params = (c_ThreadParams *) malloc( sizeof(c_ThreadParams ) );
    if (params == NULL)
        return( NULL );

    params->Timeout = timeout;
    params->ThreadFunction = threadFunction;
    params->ThreadFunctionParams = threadFunctionParams;
    c_CreateEvent( params->WakeUpEvent );
    params->State = THREAD_RUNNING;

    if ( c_CreateThread( &params->ThreadId,
                           NULL/*&attr*/,
                           c_Thread,
                           (c_ThreadArg) params ) )
        return( (c_ThreadHandle) params );

    return( NULL );
}

/** ================================================================================================
 * @brief    : stop thread
 * @note     :
 * @argumets :
 * @return   :
 **/

void c_StopThread( c_ThreadHandle handle )
{
    c_ThreadParams *params;

    params = (c_ThreadParams *) handle;
    if (params->State == THREAD_STOPPED)
        return;

    params->State = THREAD_STOPPING;
    c_SignalEvent( params->WakeUpEvent );
    while (params->State != THREAD_STOPPED)
        usleep( 1000 );
    free( params );
}

/** ================================================================================================
 * @brief    : Allow threads to terminate themselves
 * @note     :
 * @argumets :
 * @return   :
 **/
void c_QuitThread( c_ThreadHandle handle )
{
    c_ThreadParams *params;

    params = (c_ThreadParams *) handle;
    if (params->State == THREAD_STOPPED)
        return;

    params->State = THREAD_QUITTING;
    c_SignalEvent( params->WakeUpEvent );
}

/** ================================================================================================
 * @brief    : Allow threads to schedule themselves
 * @note     :
 * @argumets :
 * @return   :
 **/
int c_ChangeThisThreadInterval( c_ThreadHandle handle, unsigned long interval )
{
    c_ThreadParams *params;

    params = (c_ThreadParams *) handle;
    if (params->State == THREAD_STOPPED)
        return( 0 );

    params->Timeout = interval;
    return( 1 );
}

/** ================================================================================================
 * @brief    : Get current thread interval
 * @note     :
 * @argumets :
 * @return   :
 **/
unsigned long c_GetThreadInterval( c_ThreadHandle handle )
{
    c_ThreadParams *params = (c_ThreadParams *) handle;
    return params->Timeout;
}

/** ================================================================================================
 * @brief    : wake up thread
 * @note     :
 * @argumets :
 * @return   :
 **/

int c_WakeThread( c_ThreadHandle handle )
{
    c_ThreadParams *params;

    params = (c_ThreadParams *) handle;
    if (params->State == THREAD_STOPPED)
        return( 0 );

    c_SignalEvent( params->WakeUpEvent );
    return( 1 );
}

/** ================================================================================================
 * @brief    : thread interval
 * @note     :
 * @argumets :
 * @return   :
 **/

int c_ChangeThreadInterval( c_ThreadHandle handle, unsigned long interval )
{
    c_ThreadParams *params;

    params = (c_ThreadParams *) handle;
    if (params->State == THREAD_STOPPED)
        return( 0 );

    params->Timeout = interval;

    c_SignalEvent( params->WakeUpEvent );
    return( 1 );
}

/** ================================================================================================
 * @brief    : cvc thread
 * @note     :
 * @argumets :
 * @return   :
 **/

c_ThreadDecl c_Thread( c_ThreadArg pParams )
{
    c_ThreadParams *params;
    int              status;

    params = (c_ThreadParams *) pParams;
    if (params == NULL)
        return( (c_ThreadReturn) NULL );

    while (params->State == THREAD_RUNNING) {
        /* wakeup */
        status = c_WaitEvent( params->WakeUpEvent, params->Timeout );
        if ( status==-1 ) {
            /* error handling : wait event failed since return is -1 */
        }
        /* thread function */
        params->ThreadFunction( params->ThreadFunctionParams );
    }

    params->State = THREAD_STOPPED;
    return( (c_ThreadReturn) NULL );
}

#ifdef __GNUC__
#if !defined(MODULE_TK885) && !defined(MODULE_TK860)
#pragma GCC diagnostic pop
#endif
#endif
