//
// Copyright (c) 2015 Alexander Sacharov <a.sacharov@gmx.de>
//               All rights reserved.
//
// This work is licensed under the terms of the MIT license.
// For a copy, see <https://opensource.org/licenses/MIT>.
//

//-----------------------------------------------------------------------------
// includes <...>
//-----------------------------------------------------------------------------
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <errno.h>
#include <pthread.h>

//-----------------------------------------------------------------------------
// includes "..."
//-----------------------------------------------------------------------------
#include "iecpower/threading/cmutex.h"      /* mutex declarations */

//----------------------------------------------------------------------------
// Defines and Macros
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// Typedefs, enums, unions, variables
//----------------------------------------------------------------------------

extern int pthread_mutexattr_settype (pthread_mutexattr_t *__attr, int __kind);

/**
 * @brief    : create mutex
 * @note     :
 * @argumets :
 * @return   :
 **/

pthread_mutex_t * c_MutexCreate( pthread_mutex_t **sem )
{
   pthread_mutexattr_t initializer;
   pthread_mutex_t *newSem;

   newSem = (pthread_mutex_t *) malloc( sizeof(pthread_mutex_t) );
   if ( newSem == NULL ) {
      *sem = NULL;
      return ( NULL);
   } else {
      pthread_mutexattr_init( &initializer );
      pthread_mutexattr_settype( &initializer, PTHREAD_MUTEX_RECURSIVE_NP );
      pthread_mutex_init( newSem, &initializer );
      pthread_mutexattr_destroy( &initializer );
      *sem = newSem;
      return newSem;
   }
}

/**
 * @brief    : delete mutex
 * @note     :
 * @argumets :
 * @return   :
 **/

void c_MutexDelete( pthread_mutex_t **sem )
{
   free( *sem );
   *sem = NULL;
}

/**
 * @brief    : create condition
 * @note     :
 * @argumets :
 * @return   :
 **/

c_condition_t* c_CondCreate( c_condition_t **cond )
{
   *cond = (c_condition_t *) malloc( sizeof(c_condition_t) );

   if ( *cond == NULL )
      return (NULL);

   pthread_cond_init( &((*cond)->cond), NULL );
   pthread_mutex_init( &((*cond)->mutex), NULL );
   (*cond)->oldval = 0;
   (*cond)->newval = 0;

   return (*cond);
}

/**
 * @brief    : reset condition to begin state
 * @note     :
 * @argumets :
 * @return   :
 **/

void c_CondReset( c_condition_t *cond )
{
   pthread_mutex_lock( &cond->mutex );
   cond->oldval = 0;
   cond->newval = 0;
   pthread_cond_broadcast( &cond->cond );
   pthread_mutex_unlock( &cond->mutex );
}

/**
 * @brief    : send signal for condition
 * @note     :
 * @argumets :
 * @return   :
 **/

void c_CondSignal( c_condition_t *cond )
{
   pthread_mutex_lock( &cond->mutex );
   cond->newval++;
   /*
    * send to all threads, those are blocked by this condition
    */
   pthread_cond_broadcast( &cond->cond );
   pthread_mutex_unlock( &cond->mutex );
}

void c_CondSignalOneThread( c_condition_t *cond )
{
   pthread_mutex_lock( &cond->mutex );
   cond->newval++;
   /*
    * send to first thread, that is blocked by this condition
    */
   pthread_cond_signal( &cond->cond );
   pthread_mutex_unlock( &cond->mutex );
}

/**
 * @brief    : cond.wait
 * @note     :
 * @argumets :
 * @return   :
 **/

int c_CondWaitMs( c_condition_t *cond, unsigned long ms )
{
   struct timeval now;
   struct timespec timeout;
   int retcode;
   unsigned long sec, usec;

   pthread_mutex_lock( &cond->mutex );
   sec = ms / 1000;
   ms = ms - (sec * 1000);
   gettimeofday( &now, NULL );
   timeout.tv_sec = now.tv_sec + sec;

   /* wait event! */
   usec = (ms * 1000L) + now.tv_usec;
   if ( usec >= 1000000L ) {
      usec -= 1000000L;
      timeout.tv_sec++;
   }
   timeout.tv_nsec = usec * 1000;

   if ( cond->oldval == cond->newval )
      retcode = pthread_cond_timedwait( &cond->cond, &cond->mutex, &timeout ) != ETIMEDOUT;
   if ( cond->oldval != cond->newval ) {
      cond->oldval = cond->newval;
      retcode = 1;
   }
   pthread_mutex_unlock( &cond->mutex );

   return (retcode);
}

int c_CondWaitUsec( c_condition_t *cond, unsigned long mks )
{
   struct timespec now;
   struct timespec timeout;
   int retcode;
   unsigned long sec, usec;

   pthread_mutex_lock( &cond->mutex );

   // get time
   clock_gettime( CLOCK_REALTIME_COARSE, &now );

   sec = mks / 1000000ul;
   usec = mks - sec * 1000000ul;
   //
   timeout.tv_sec  = now.tv_sec + sec;
   timeout.tv_nsec = now.tv_nsec + usec * 1000;

   if ( timeout.tv_nsec > 1000000000l ) {
      timeout.tv_nsec -= 1000000000l;
      timeout.tv_sec++;
   }

   if ( cond->oldval == cond->newval )
      retcode = pthread_cond_timedwait( &cond->cond, &cond->mutex, &timeout ) != ETIMEDOUT;
   if ( cond->oldval != cond->newval ) {
      cond->oldval = cond->newval;
      retcode = 1;
   }
   pthread_mutex_unlock( &cond->mutex );

   return (retcode);
}

/**
 * @brief    : delete condition
 * @note     :
 * @argumets :
 * @return   :
 **/

void c_CondDelete( c_condition_t **cond )
{
   if ( *cond != NULL )
      free( *cond );
   *cond = NULL;
}

