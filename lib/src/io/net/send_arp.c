//
// Copyright (c) 2020 Alexander Sacharov and project contributors.
//               All rights reserved.
//
// This work is licensed under the terms of the MIT license.
// For a copy, see <https://opensource.org/licenses/MIT>.
//

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>           // close()
#include <string.h>           // strcpy, memset(), and memcpy()
#include <sys/types.h>        // needed for socket(), uint8_t, uint16_t
#include <sys/socket.h>       // needed for socket()
#include <netinet/in.h>       // IPPROTO_RAW
#include <netinet/ip.h>       // IP_MAXPACKET (which is 65535)
#include <sys/ioctl.h>        // macro ioctl is defined
#include <bits/ioctls.h>      // defines values for argument "request" of ioctl.
#include <net/if.h>           // struct ifreq
#include <linux/if_ether.h>   // ETH_P_ARP = 0x0806
#include <linux/if_packet.h>  // struct sockaddr_ll (see man 7 packet)
#include <net/ethernet.h>

#include <errno.h>            // errno, perror()

#include "iecpower/io/net/send_arp.h"

// Define a struct for ARP header
typedef struct _arp_hdr arp_header;

struct _arp_hdr {
   uint16_t htype;
   uint16_t ptype;
   uint8_t hlen;
   uint8_t plen;
   uint16_t opcode;
   uint8_t sender_mac[6];
   uint8_t sender_ip[4];
   uint8_t target_mac[6];
   uint8_t target_ip[4];
};

// Define some constants.
#define PROTO_ARP           0x0806
#define ETH_HDRLEN          14      // Ethernet header length
#define IP4_HDRLEN          20      // IPv4 header length
#define ARP_HDRLEN          28      // ARP header length
#define ARPOP_REQUEST        1      // Taken from <linux/if_arp.h>
#define BUF_SIZE            60

/*************************************************************************//**
 * Send IPv4 ARP Packet.
 * Send an IPv4 ARP packet via raw socket at the link layer (ethernet frame).
 * Values set for ARP request.
 * @param interface
 * @param senderIp
 * @param targetIp
 * @param waitRspMs
 * @param verbose
 * @param stream
 * @return  0 if successfully, otherwise -1 if an error occurred
 */
int send_arp(const char *interface,
             const char *senderIp, const char *targetIp,
             uint32_t waitRspMs,
             uint8_t verbose, FILE *stream) {
   int i, frame_length, sd, bytes;
   arp_header arphdr;
   uint8_t src_ip[4], src_mac[6], dst_mac[6], ether_frame[IP_MAXPACKET];
   struct sockaddr_in ipv4;
   struct sockaddr_ll device;
   struct ifreq ifr;

   if (!interface || !senderIp || !targetIp || !stream) {
      perror("arp_sendto: null arguments\n");
      return (-1);
   }

   // Interface to send packet through.

   // Submit request for a socket descriptor to look up interface.
   if ((sd = socket(AF_INET, SOCK_RAW, IPPROTO_RAW)) < 0) {
      perror("arp_sendto: socket() failed to get socket descriptor for using ioctl()");
      return (-1);
   }

   // Use ioctl() to look up interface name and get its IPv4 address.
   memset(&ifr, 0, sizeof(ifr));
   snprintf(ifr.ifr_name, sizeof(ifr.ifr_name), "%s", interface);
   if (ioctl(sd, SIOCGIFADDR, &ifr) < 0) {
      perror("arp_sendto: ioctl() failed to get source IP address");
      return (EXIT_FAILURE);
   }

   // Copy source IP address.
   memcpy(&ipv4, &ifr.ifr_addr, sizeof(struct sockaddr_in));
   memcpy(src_ip, &ipv4.sin_addr, 4 * sizeof(uint8_t));

   // Use ioctl() to look up interface name and get its MAC address.
   memset(&ifr, 0, sizeof(ifr));
   snprintf(ifr.ifr_name, sizeof(ifr.ifr_name), "%s", interface);
   if (ioctl(sd, SIOCGIFHWADDR, &ifr) < 0) {
      perror("arp_sendto: ioctl() failed to get source MAC address");
      return (EXIT_FAILURE);
   }

   close(sd);

   // Copy source MAC address.
   memcpy(src_mac, ifr.ifr_hwaddr.sa_data, 6 * sizeof(uint8_t));

   // Report source MAC address to stdout.
   if (verbose) {
      fprintf(stream, "MAC address for interface %s is", interface);
      for (i = 0; i < 5; i++) {
         fprintf(stream, "%02x:", src_mac[i]);
      }
      fprintf(stream, "%02x\n", src_mac[5]);
   }

   // Find interface index from interface name and store index in
   // struct sockaddr_ll device, which will be used as an argument of sendto().
   if ((device.sll_ifindex = if_nametoindex(interface)) == 0) {
      perror("if_nametoindex() failed to obtain interface index");
      return (-1);
   }

   if (verbose)
      fprintf(stream, "Index for interface %s is %i\n", interface, device.sll_ifindex);

   // Set destination MAC address: broadcast address
   memset(dst_mac, 0xff, 6 * sizeof(uint8_t));

   memcpy(&arphdr.sender_ip, src_ip, 4 * sizeof(uint8_t));
   memcpy(&arphdr.target_ip, src_ip, 4 * sizeof(uint8_t));

   // Fill out sockaddr_ll.
   device.sll_family = AF_PACKET;
   memcpy(device.sll_addr, src_mac, 6 * sizeof(uint8_t));
   device.sll_halen = htons(6);

   // ARP header

   // Hardware type (16 bits): 1 for ethernet
   arphdr.htype = htons(1);

   // Protocol type (16 bits): 2048 for IP
   arphdr.ptype = htons(ETH_P_IP);

   // Hardware address length (8 bits): 6 bytes for MAC address
   arphdr.hlen = 6;

   // Protocol address length (8 bits): 4 bytes for IPv4 address
   arphdr.plen = 4;

   // OpCode: 1 for ARP request
   arphdr.opcode = htons(ARPOP_REQUEST);

   // Sender hardware address (48 bits): MAC address
   memcpy(&arphdr.sender_mac, src_mac, 6 * sizeof(uint8_t));

   // Target hardware address (48 bits): zero
   memset(&arphdr.target_mac, 0, 6 * sizeof(uint8_t));

   // Fill out ethernet frame header.

   // Ethernet frame length = ethernet header (MAC + MAC + ethernet type) + ethernet data (ARP header)
   frame_length = 6 + 6 + 2 + ARP_HDRLEN;

   // Destination and Source MAC addresses
   memcpy(ether_frame, dst_mac, 6 * sizeof(uint8_t));
   memcpy(ether_frame + 6, src_mac, 6 * sizeof(uint8_t));

   // Next is ethernet type code (ETH_P_ARP for ARP).
   // http://www.iana.org/assignments/ethernet-numbers
   ether_frame[12] = ETH_P_ARP / 256;
   ether_frame[13] = ETH_P_ARP % 256;

   // Next is ethernet frame data (ARP header).

   // ARP header
   memcpy(ether_frame + ETH_HDRLEN, &arphdr, ARP_HDRLEN * sizeof(uint8_t));

   // Submit request for a raw socket descriptor.
   if ((sd = socket(PF_PACKET, SOCK_RAW, htons(ETH_P_ALL))) < 0) {
      perror("arp_sendto: socket() failed");
      return (-1);
   }

   // Send ethernet frame to socket.
   if ((bytes = sendto(sd, ether_frame, frame_length, 0, (struct sockaddr *) &device,
                       sizeof(device))) <= 0) {
      perror("arp_sendto: sendto() failed");
      return (-1);
   }

   if (waitRspMs) {
      // receive
      unsigned char buffer[BUF_SIZE];
      struct timeval read_timeout;
      struct ethhdr *rcv_resp = (struct ethhdr *) buffer;
      arp_header *arp_resp = (arp_header *) (buffer + ETH_HDRLEN);

      memset(buffer, 0x00, 60);

      read_timeout.tv_sec = waitRspMs / 1000;
      read_timeout.tv_usec = read_timeout.tv_sec * 1000000 - waitRspMs * 1000;

      setsockopt(sd, SOL_SOCKET, SO_RCVTIMEO, &read_timeout, sizeof(read_timeout));

      if (verbose)
         fprintf(stream, " RECEIVED ARP RCVTIMEO=%d ms\n", waitRspMs);

      frame_length = recvfrom(sd, buffer, BUF_SIZE, 0, NULL, NULL);
      if (frame_length == -1) {
         perror("arp_sendto: recvfrom()");
         return (-1);
      }

      if (htons(rcv_resp->h_proto) == PROTO_ARP) {
         if (verbose) {
            //if( arp_resp->opcode == ARP_REPLY )
            fprintf(stream, " RECEIVED ARP RESP len=%d\n", frame_length);
            fprintf(stream, " Sender IP :");
            for (i = 0; i < 4; ++i)
               fprintf(stream, "%u.", arp_resp->sender_ip[i]);

            fprintf(stream, "\n Sender MAC :");
            for (i = 0; i < 6; ++i)
               fprintf(stream, " %02X:", arp_resp->sender_mac[i]);

            fprintf(stream, "\nReceiver IP :");
            for (i = 0; i < 4; ++i)
               fprintf(stream, " %u.", arp_resp->target_ip[i]);

            fprintf(stream, "\n Self MAC :");
            for (i = 0; i < 6; ++i)
               fprintf(stream, " %02X:", arp_resp->target_mac[i]);

            fprintf(stream, "\n  :");
         }
      }
      else {
         if (verbose) fprintf(stream, " ARP RESP losts. Frame length = %d\n", frame_length);
      }
   }

   // Close socket descriptor.
   close(sd);

   return (0);
}
