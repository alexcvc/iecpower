//
// Copyright (c) 2020 Alexander Sacharov and project contributors.
//               All rights reserved.
//
// This work is licensed under the terms of the MIT license.
// For a copy, see <https://opensource.org/licenses/MIT>.
//

/**
 * Handling
 * @created : 2012-09-26
 * @changes :
 *
 * This file contains the following routines:
 *
 */

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wformat="
#pragma GCC diagnostic ignored "-Wunused-parameter"

//#define _GNU_SOURCE     /* To get defns of NI_MAXSERV and NI_MAXHOST */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "iecpower/io/net/net_ifaces.h"
#include "iecpower/io/net/send_arp.h"

/**
 * @brief    : Grabs local network interface information and stores in a net_iface_t defined in network.h
 * @arguments : socket, net_iface_t
 * @return   : 0 on success -1 on failure
 **/
static int get_local_adapter_info(int rsock, net_iface_t *ifcfg)
{
   struct ifreq ifr;
   struct sockaddr_in sain;

   memset( &ifr, 0, sizeof(ifr) );
   strncpy( ifr.ifr_name, ifcfg->name, IF_NAMESIZE );
   if ( (ioctl( rsock, SIOCGIFHWADDR, &ifr )) == -1 ) {
      perror( "ioctl SIOCGIFHWADDR fault" );
      return -1;
   }

   memcpy( &(ifcfg->mac), &ifr.ifr_hwaddr.sa_data, 6 );

   memset( &ifr, 0, sizeof(ifr) );
   strncpy( ifr.ifr_name, ifcfg->name, IF_NAMESIZE );
   if ( (ioctl( rsock, SIOCGIFADDR, &ifr )) == -1 ) {
      perror( "ioctl SIOCGIFADDR fault" );
      return -1;
   }

   // make ARM-compiler happy!
   memcpy( &sain, &ifr.ifr_addr, sizeof(struct sockaddr_in) );
   memcpy( &ifcfg->ip, &sain.sin_addr, 4 );

   memset( &ifr, 0, sizeof(ifr) );
   strncpy( ifr.ifr_name, ifcfg->name, IF_NAMESIZE );
   if ( (ioctl( rsock, SIOCGIFBRDADDR, &ifr )) == -1 ) {
      perror( "ioctl SIOCGIFBRDADDR fault" );
      return -1;
   }

    // make ARM-compiler happy!
    memcpy( &sain, &ifr.ifr_broadaddr, sizeof(struct sockaddr_in));
    memcpy(&ifcfg->bc, &sain.sin_addr, 4);

    memset(&ifr, 0, sizeof(ifr));
    strncpy(ifr.ifr_name, ifcfg->name, IF_NAMESIZE);
    if((ioctl(rsock, SIOCGIFNETMASK, &ifr)) == -1) {
        perror("ioctl SIOCGIFNETMASK fault" );
        return -1;
    }

    // make ARM-compiler happy!
    memcpy( &sain, &ifr.ifr_netmask, sizeof(struct sockaddr_in));
    memcpy(&ifcfg->nm.s_addr, &sain.sin_addr, 4);

    memset(&ifr, 0, sizeof(ifr));
    strncpy(ifr.ifr_name, ifcfg->name, IF_NAMESIZE);
    if((ioctl(rsock, SIOCGIFMTU, &ifr)) == -1) {
        perror( "ioctl SIOCGIFMTU fault");
        return -1;
    }
    ifcfg->mtu = ifr.ifr_mtu;

    return 0;
}

/**
 * @brief    :
 * @arguments :
 * @return   : 0 on success -1 on failure
 **/
int net_get_ifaces( net_iface_list_t *iflist, uint8_t verbose, char* outbuf, size_t len )
{
   struct ifreq  ifr;
   struct ifconf ifc;
   char          buf[1024];
   int           sock;

   iflist->number = 0;
   sock = socket( AF_INET, SOCK_DGRAM, IPPROTO_IP );
   if ( sock == -1 ) {
      /* handle error*/
      perror( "socket open fault" );
      return (-1);
   };

   ifc.ifc_len = sizeof(buf);
   ifc.ifc_buf = buf;
   if ( ioctl( sock, SIOCGIFCONF, &ifc ) == -1 ) {
      /* handle error */
      perror( "ioctl SIOCGIFCONF fault" );
      return (-1);
   }

   struct ifreq* it = ifc.ifc_req;
   const struct ifreq* const end = it + (ifc.ifc_len / sizeof(struct ifreq));

   for ( ; it != end; ++it ) {
      /* copy name */
      strcpy( ifr.ifr_name, it->ifr_name );
      if ( ioctl( sock, SIOCGIFFLAGS, &ifr ) == 0 ) {
         if ( !(ifr.ifr_flags & IFF_LOOPBACK) ) {  // don't count loopback

            /* we have to get info */
            strcpy( iflist->ifc[iflist->number].name, it->ifr_name );
            if ( get_local_adapter_info( sock, (net_iface_t*) &iflist->ifc[iflist->number] ) != -1 )
               iflist->number++;
            if ( iflist->number == IFMAXNUM )
               break;
         }
      } else {
         /* handle error */
         perror( "ioctl SIOCGIFFLAGS fault" );
         return (-1);
      }
   }

   if( verbose && outbuf && len) {
      int i;
      unsigned long addr;
      /* clear */
      memset(outbuf, 0, len );

      for ( i = 0; i < iflist->number; i++ ) {
         sprintf(buf, "MAC %-6s: %02x:%02x:%02x:%02x:%02x:%02x\n",
                 iflist->ifc[i].name,
                 iflist->ifc[i].mac.ether_addr_octet[0],
                 iflist->ifc[i].mac.ether_addr_octet[1],
                 iflist->ifc[i].mac.ether_addr_octet[2],
                 iflist->ifc[i].mac.ether_addr_octet[3],
                 iflist->ifc[i].mac.ether_addr_octet[4],
                 iflist->ifc[i].mac.ether_addr_octet[5] );

         strncpy(outbuf, buf, len-1);

         addr = ntohl(iflist->ifc[i].ip.s_addr);

         sprintf(buf, " IP \"%u.%u.%u.%u\"" ,
                 ((addr & 0xff000000)>>24),
                 ((addr & 0x00ff0000)>>16),
                 ((addr & 0x0000ff00)>>8 ),
                 ((addr & 0x000000ff)) );

         addr = ntohl(iflist->ifc[i].bc.s_addr);

         sprintf(buf, " BC \"%u.%u.%u.%u\"" ,
                 ((addr & 0xff000000)>>24),
                 ((addr & 0x00ff0000)>>16),
                 ((addr & 0x0000ff00)>>8 ),
                 ((addr & 0x000000ff)) );

         if( strlen(buf) < (len - strlen(outbuf)) ) {
            strcat(outbuf, buf);
         }

         addr = ntohl(iflist->ifc[i].nm.s_addr);

         sprintf(buf, " NM \"%u.%u.%u.%u\" MTU \"%d\"\n" ,
                 ((addr & 0xff000000)>>24),
                 ((addr & 0x00ff0000)>>16),
                 ((addr & 0x0000ff00)>>8 ),
                 ((addr & 0x000000ff) ),
                 iflist->ifc[i].mtu ); // Maximum Transmission Unit

         if( strlen(buf) < (len - strlen(outbuf)) ) {
            strcat(outbuf, buf);
         }

      }
   }

   return (0);
}

int net_get_mac_address(const char *name, struct ether_addr *mac)
{
   struct ifreq ifreq;
   int err, fd;

   memset(&ifreq, 0, sizeof(ifreq));
   strncpy(ifreq.ifr_name, name, sizeof(ifreq.ifr_name) - 1);

   fd = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
   if (fd < 0) {
      perror("socket failed");
      return -1;
   }

   err = ioctl(fd, SIOCGIFHWADDR, &ifreq);
   if (err < 0) {
      perror("ioctl SIOCGIFHWADDR failed");
      close(fd);
      return -1;
   }

   close(fd);

   /* Get mac */
   memcpy( mac, &ifreq.ifr_hwaddr.sa_data, ETH_ALEN );

   return 0;
}

/**
 * @brief find local IP/netmask by remote server host
 * @param ipHost - remote server
 * @param localIp - IP of local interface (network order)
 * @param localNetmask - netmask for IP (network order)
 * @param ethernet name
 * @param ethernet name length
 * @return 0 if successfully, otherwise - -1
 */
int net_resolve_local_interface( const uint32_t remoteHost, uint32_t* localIp, uint32_t* localNetmask, char *ethName, int lenEthName )
{
   if (!ethName || !localIp || !localNetmask) {
      perror("net_resolve_local_inet: arguments error");
   }
   else {
      net_iface_list_t ifl;
      int i;

      ethName[0] = '\0';
      *localIp = INADDR_ANY;            /* IP of local interface (network order) */
      *localNetmask = INADDR_NONE;      /* netmask for IP (network order) */

      ifl.number = 0;
      if (-1 == net_get_ifaces(&ifl, 0, NULL, 0)) {
         perror("Failed to get local IP-address in net_get_ifaces");
         return (-1);
      }
      else {
         for (i = 0; i < ifl.number; i++) {
            uint32_t ip = ifl.ifc[i].ip.s_addr;
            uint32_t nm = ifl.ifc[i].nm.s_addr;
            if ((ip != INADDR_ANY) && (nm != INADDR_NONE)) {
               if (((ntohl(remoteHost) ^ ntohl(ip)) & ntohl(nm)) == 0) {
                  strncpy(ethName, ifl.ifc[i].name, lenEthName);
                  *localIp = ip;
                  *localNetmask = nm;
                  return 0;
               }
            }
         }
      }
   }
   return (-1);
}

/**
 * @brief   gets IP-address of name on network order.
 * @param[in] name
 * @return  Return IP-address of name on network order, otherwise - 0.
 * @author alexander Apr 23, 2017
 */

uint32_t net_resolve_ip_network_order (const char *name)
{
   struct   hostent *he;
   uint32_t ip = inet_addr( name );

   if ( ip != INADDR_NONE ) {
      return (ip);
   }

   he = gethostbyname( name );

   if ( !he ) {
      return (0);
   }

   memcpy( &ip, &he->h_addr, sizeof(uint32_t));
   return (ip);
}

/**
 * @brief Check if host address 'ip' replies on ARP
 * @param ip - remote IP
 * @return 0 if successfully, otherwise -1 if an error occurred
 */
int net_check_arp_reply_from_host (const  char *ethName, uint32_t ip, uint32_t ipLan, uint32_t netmaskLan)
{
   // ARP
   struct sockaddr_in saSrc, saDst;
   char strSrc[INET_ADDRSTRLEN];
   char strDst[INET_ADDRSTRLEN];

   saSrc.sin_addr.s_addr = ipLan;
   saDst.sin_addr.s_addr = netmaskLan;

   // now get it back and print it
   inet_ntop(AF_INET, &(saSrc.sin_addr), strSrc, INET_ADDRSTRLEN);
   inet_ntop(AF_INET, &(saDst.sin_addr), strDst, INET_ADDRSTRLEN);

   if (send_arp( ethName, strSrc, strDst, 0, false, stderr ) == -1 )
      return ( -1 );

   return ( 0 );
}
