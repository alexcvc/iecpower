//
// Copyright (c) 2020 Alexander Sacharov and project contributors.
//               All rights reserved.
//
// This work is licensed under the terms of the MIT license.
// For a copy, see <https://opensource.org/licenses/MIT>.
//

/* Linux socket definitions */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdarg.h>
#include <time.h>
#include <sys/timeb.h>
#include <sys/time.h>
#include <sys/types.h>
#include <errno.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netinet/ip.h> /* superset of previous */
#include <netinet/tcp.h>
#include <sys/uio.h>
#include "iecpower/io/net/net_ifaces.h"
#include "iecpower/io/net/send_arp.h"
#include "iecpower/logging/details/logsyslog.h"

#if defined(DEBUG) && DEBUG == 1
#define DBG(...) do { fprintf(stderr, "%s: ", __func__); fprintf(stderr, __VA_ARGS__); } while (0)
#else
#define DBG(...)
#endif

#define SOCKET              int
#define SOCKADDR_IN         struct sockaddr_in
#define SOCKADDR_LEN        sizeof(struct sockaddr_in)
#define SOCKADDR_LEN_TYPE   socklen_t
#define LPSOCKADDR          struct sockaddr *
#define SOCKET_ERROR        -1
#define INVALID_SOCKET      -1
#define CLOSESOCKET( s )    close(s)
#define IOCTL_SOCKET(fd, func, arg) ioctl(fd, func, arg)
#define GET_LAST_ERROR( )   errno
#define SYSLOG_BUF_SIZE     2048
#define SYSLOG_FRM_BUF_SIZE 512
#define MAX_PATH            256
#define MAX_HOSTNAME        128

#define  INTERNALLOG LOG_ERR|LOG_CONS|LOG_PERROR|LOG_PID

static SOCKET      logSock     = INVALID_SOCKET;  /* UDP socket for log-daemon */
static uint32_t    logHostAddr = 0UL;             /* IP-address of log-daemon (network order) */
static char       *logHostName = NULL;            /* name of syslogd host ("localhost") */
static uint16_t    logPort     = 514;             /* port in host order */
static bool        logOpened   = false;           /* have done logclient_openlog() */
static int         logStat     = 0;               /* status bits, set by logclient_openlog() */
static char       *logTag      = NULL;            /* string to tag the entry with */
static int         logFacil    = LOG_USER;        /* default facility code */
static int         logMask     = 0xFF;            /* mask of priorities to be logged */

static char        ethName[16];
static char       *myHostName  = NULL;            /* name of syslogd host ("localhost") */
static uint32_t    nlMyIPAddr  = INADDR_ANY;      /* IP of local interface (network order) */
static uint32_t    nlNetMask   = INADDR_NONE;     /* netmask for IP (network order) */
static char        err_buf [256];
extern char       *__progname;                    /* Program name, from crt0. */

/*************************************************************************//**
 * @brief   creates an RFC5424-conform log-message
 * @param[in]
 * @return (0) - if successfully, otherwise (-1) in case of an error.
 *****************************************************************************/
int logclient_syslog( int pri, const char *fmt, ... )
{
   int      rc;
   va_list  args;

   va_start( args, fmt );
   rc = logclient_vsyslog( pri, fmt, args );
   va_end( args );

   return (rc);
}

/*************************************************************************//**
 * @brief   creates an RFC5424-conform log-message
 * @param[in]
 * @return (0) - if successfully, otherwise (-1) in case of an error.
 *****************************************************************************/
int logclient_vsyslog( int pri, register const char *fmt, va_list ap )
{
   int            cnt;
   char           ch;
   register char  *p, *t;
   struct timeb   now;
   struct in_addr ip_addr;
   int            saved_errno;
   char           *stdp = NULL;
   char           buf[SYSLOG_BUF_SIZE];
   char           fmt_cpy[SYSLOG_FRM_BUF_SIZE];
   int            rc = 0;

   if ( !fmt || (strlen( fmt ) > (sizeof(fmt_cpy) - 2)) )
      return (-1);

   /* Check for invalid bits. */
   if ( pri & ~(LOG_PRIMASK | LOG_FACMASK) ) {
      pri &= LOG_PRIMASK | LOG_FACMASK;
   }

   /* Check priority against setlogmask values. */
   if ( (!LOG_MASK( LOG_PRI(pri) )) & logMask ) {
      return 0;
   }

   saved_errno = errno;

   /* Set default facility if none specified. */
   if ( (pri & LOG_FACMASK) == 0 )
      pri |= logFacil;

   ftime( &now );

   /* Build the message. */
   memset(buf, 0, sizeof(buf));

   /*
    * priority
    */
   p = buf + sprintf( buf, "<%d>1 ", pri );

   /*
    * timestamp
    */
   p += strftime( p, sizeof(buf) - (p - buf), "%Y-%m-%dT%H:%M:%S", gmtime( &now.time ) );
   p += sprintf( p, ".%03dZ ", now.millitm);

   /*
    * host name
    */
   if( myHostName == NULL ) {
      myHostName = (char*)malloc(MAX_HOSTNAME);
      memset(myHostName, 0, MAX_HOSTNAME);
      gethostname(myHostName, MAX_HOSTNAME-1);
   }

#if 1
   /* after WB request (mandatory) 2019-11-06 */
   if ( myHostName && strlen(myHostName) ) {
      p += sprintf( p, "%s ", myHostName );
   } else {
      ip_addr.s_addr = nlMyIPAddr;
      p += sprintf( p, "%s ", inet_ntoa( ip_addr ) );
   }
#else
   if ( myHostName && strlen(myHostName) ) {
      p += sprintf( p, "%s", myHostName );
   }
   ip_addr.s_addr = nlMyIPAddr;
   p += sprintf( p, "(%s) ", inet_ntoa( ip_addr ) );
#endif

   /*
    * program name
    */
   if ( logTag == NULL )
      logTag = __progname;

   p += sprintf( p, "%s ", logTag );

   // ... and the ID of the caller to the header
   p += sprintf( p, "%d 0 - ", getpid() );

   /* Substitute error message for %m. */
   for ( t = fmt_cpy; (ch = *fmt); ++fmt )
      if ( ch == '%' && (fmt[1] == 'm' || fmt[1] == 'M') ) {
         ++fmt;
         t += sprintf( t, "%s", strerror( saved_errno ) );
      } else if(ch == '\n') {
         *t++ = ';';
      } else if(ch == '\n') {
         *t++ = ';';
      } else {
         *t++ = ch;
      }
   *t = '\0';

   p += vsprintf( p, fmt_cpy, ap );
   cnt = p - buf;

   /* Output to stderr if requested. */
   if ( logStat & LOG_PERROR ) {
      struct iovec iov[2];
      register struct iovec *v = iov;

      v->iov_base = stdp;
      v->iov_len = cnt - (stdp - buf);
      ++v;
      v->iov_base = "\n";
      v->iov_len = 1;
      (void) writev( STDERR_FILENO, iov, 2 );
   }

   /* Get connected, output the message to the local logger. */
   if ( !logOpened ) {
      rc = logclient_openlog( logHostName, logPort, logTag, logStat | LOG_NDELAY, logFacil );
   }

   if ( logSock != INVALID_SOCKET ) {
      struct sockaddr_in addr;

      memset( &addr, 0, sizeof(addr) );
      addr.sin_family = AF_INET;
      addr.sin_addr.s_addr = logHostAddr;
      addr.sin_port = htons( logPort );

      if ( sendto( logSock, buf, cnt, 0, (struct sockaddr*) &addr, sizeof(addr) ) < 0 ) {
         perror( "UDP-write failed: " );
         close( logSock );
         logSock = INVALID_SOCKET;
         rc = -1;
      }
   }
   /*
    * Output the message to the console; don't worry about blocking,
    * if console blocks everything will.  Make sure the error reported
    * is the one from the syslogd failure.
    */
   if ( (logStat & LOG_PERROR) || ((logStat & LOG_CONS) && rc == -1) ) {
      fputs( buf, stdout );
   }

   return (rc);
}

/*************************************************************************//**
 * @brief   init socket.
 * @details initialization.
 *
 * @param[in]
 *
 * @return  true if successfully, otherwise false if an error occurred
 * @author alexander Apr 23, 2017
 *****************************************************************************/
static bool sock_init (uint32_t ipHost)
{
   static bool done = false;
   static bool rc = false;

   if ( done ) {
      return (rc);
   } else {
      net_iface_list_t ifl;
      int i;

      done = 1;

      ethName[0] = '\0';

      ifl.number = 0;
      if ( -1 == net_get_ifaces( &ifl, 0, NULL, 0 ) ) {
         perror( "Failed to get local IP-address" );
         return ( false);
      } else {
         for ( i = 0; i < ifl.number; i++ ) {
            uint32_t ip = ifl.ifc[i].ip.s_addr;
            uint32_t nm = ifl.ifc[i].nm.s_addr;
            if ( (ip != INADDR_ANY) && (nm != INADDR_NONE) ) {
               if ( ((ntohl( ipHost ) ^ ntohl( ip )) & ntohl( nm )) == 0 ) {
                  strcpy( ethName, ifl.ifc[i].name );
                  nlMyIPAddr = ip;
                  nlNetMask  = nm;
                  break;
               }
            }
         }
      }
   }
   rc = true;
   return (rc);
}

/*************************************************************************//**
 * @brief   gets IP-address of name on network order .
 * @details returns IP-address of name on network order .
 *
 * @param[in]
 *
 * @return  Return IP-address of name on network order .
 * @author alexander Apr 23, 2017
 *****************************************************************************/
static uint32_t resolve (const char *name)
{
   struct   hostent *he;
   uint32_t ip = inet_addr( name );

   if ( ip != INADDR_NONE ) {
      return (ip);
   }

   he = gethostbyname( name );

   if ( !he ) {
      snprintf( err_buf, sizeof(err_buf), "Unknown host '%s'", name );
      return (0);
   }

   memcpy( &ip, &he->h_addr, sizeof(uint32_t));
   return (ip);
}

/*
 * Check if host address 'ip' replies on ARP.
 */
static bool check_arp ()
{
   // ARP
   struct sockaddr_in saSrc, saDst;
   char strSrc[INET_ADDRSTRLEN];
   char strDst[INET_ADDRSTRLEN];

   saSrc.sin_addr.s_addr = nlMyIPAddr;
   saDst.sin_addr.s_addr = logHostAddr;

   // now get it back and print it
   inet_ntop(AF_INET, &(saSrc.sin_addr), strSrc, INET_ADDRSTRLEN);
   inet_ntop(AF_INET, &(saDst.sin_addr), strDst, INET_ADDRSTRLEN);

   if (send_arp( ethName, strSrc, strDst, 0, false, stderr ) == -1 )
      return ( false );

   return ( true );
}

/*************************************************************************//**
 * @brief   Return true if 'ip' is on this LAN..
 * @details .
 *
 * @param[in]
 *
 * @return  .
 * @author alexander Apr 23, 2017
 *****************************************************************************/
static bool is_on_LAN (uint32_t ipHost)
{
   return (((ntohl( ipHost ) ^ ntohl( nlMyIPAddr )) & ntohl( nlNetMask )) == 0);
}

/*************************************************************************//**
 * @brief   Return true if 'ip' is a (directed) ip-broadcast address..
 * @details .
 *
 * @param[in]
 *
 * @return  .
 * @author alexander Apr 23, 2017
 *****************************************************************************/
static bool is_broadcast (uint32_t ipHost)
{
   return ((~ntohl( ipHost ) & ~ntohl( nlNetMask )) == 0);
}

/*************************************************************************//**
 * @brief   Open a UDP "connection" to log-host.
 * @details .
 *
 * @param[in]
 *
 * @return  .
 * @author alexander Apr 23, 2017
 *****************************************************************************/
static int openloghost (const char *hostName)
{
   logHostAddr = resolve( hostName );
   if ( !sock_init( logHostAddr ) )
      return (-1);

   if ( logSock != INVALID_SOCKET ) {
      close( logSock );
      logSock = INVALID_SOCKET;
   }

   if ( !logHostAddr ) {
      fprintf( stderr, "logHostAddr: resolve failed\n");
      return (-1);
   }

   if ( !is_on_LAN( logHostAddr ) ) {
      fprintf( stderr, "is_on_LAN: failed\n");
      return (-1);
   }

   if ( is_broadcast( logHostAddr ) ) {
      fprintf( stderr, "is_broadcast failed\n");
      return (-1);
   }

   if ( !check_arp() ) {
      fprintf( stderr, "check_arp failed\n");
      return (-1);
   }

   logSock = socket( AF_INET, SOCK_DGRAM, 0 );
   if ( logSock == INVALID_SOCKET ) {
      perror( "socket() failed" );
      return (-1);
   }

   if ( is_broadcast( logHostAddr ) ) {
      bool on = true;
      setsockopt( logSock, SOL_SOCKET, SO_BROADCAST, (const char*) &on, sizeof(on) );
   }
   return (0);
}

/*************************************************************************//**
 * @brief   .
 * @details .
 *
 * @param[in]
 *
 * @return  .
 * @author alexander Apr 23, 2017
 *****************************************************************************/
int logclient_openlog ( const char *logIp, uint16_t port, const char *ident, int options, int logfac)
{
   int rc = -1; /* pessimistic */

   if ( !logIp || !strcmp( logIp, "0.0.0.0" ) ) /* user didn't want logging to daemon */
   {
      return (-1);
   }

   logHostName = strdup( logIp );

   if ( !port )
      logPort = 514;
   else
      logPort = port;

   if ( ident )
      logTag = (char*) ident;

   logStat = options;

   if ( logfac && (logfac & ~LOG_FACMASK) == 0 )
      logFacil = logfac;

   if ( LOG_PRI( logfac ) )
      logMask = LOG_PRI( logfac );

   if ( logMask )
      logclient_setlogmask( logMask );

   if ( !(logStat & LOG_NDELAY) ) {
      return (0);
   }

   logOpened = true; /* prevent recursion below */

   rc = openloghost( logHostName );
   if ( rc >= 0 ) {
      logclient_syslog( LOG_INFO, "SysLog UDP started in \"%s\". Host:%s Port:%d", __progname, logHostName, logPort );
   }
   return (rc);
}

/*************************************************************************//**
 * @brief   .
 * @details .
 *
 * @param[in]
 *
 * @return  .
 * @author alexander Apr 23, 2017
 *****************************************************************************/
int logclient_closelog (void)
{
   if ( logSock != INVALID_SOCKET )
      close( logSock );
   if ( logHostName )
      free( (void*) logHostName );
   if ( myHostName )
      free( (void*) myHostName );

   logSock = INVALID_SOCKET;
   logHostName = NULL;
   myHostName = NULL;
   logOpened = false;
   err_buf[0] = '\0';
   return (0);
}

/*************************************************************************//**
 * @brief   .
 * @details .
 *
 * @param[in]
 *
 * @return  .
 * @author alexander Apr 23, 2017
 *****************************************************************************/
int logclient_setlogmask (int pri_mask)
{
   int old = logMask;
   if ( pri_mask )
      logMask = pri_mask;
   return (old);
}

/*************************************************************************//**
 * @brief   .
 * @details .
 *
 * @param[in]
 *
 * @return  .
 * @author alexander Apr 23, 2017
 *****************************************************************************/
char *setlogtag ( char *new )
{
   char *old = logTag;
   logTag = new;
   return (old);
}

/*************************************************************************//**
 * @brief   .
 * @details .
 *
 * @param[in]
 *
 * @return  .
 * @author alexander Apr 23, 2017
 *****************************************************************************/
const char *syslog_strerror (void)
{
   return (err_buf);
}

/*************************************************************************//**
 * @brief   .
 * @details .
 *
 * @param[in]
 *
 * @return  .
 * @author alexander Apr 23, 2017
 *****************************************************************************/
const char *syslog_loghost (const char *host)
{
   if ( logHostName )
      free( logHostName );
   logHostName = NULL;
   if ( !host )
      return (NULL);

   logHostName = strdup( host );
   if ( !strcmp( host, "0.0.0.0" ) )
      return (logHostName);

   if ( !resolve( host ) )
      return (NULL);
   if ( !sock_init( resolve( host ) ) )
      return (NULL);
   return (logHostName);
}
