//
// Copyright (c) 2011 Alexander Sacharov <a.sacharov@gmx.de>
//               All rights reserved.
//
// This work is licensed under the terms of the MIT license.
// For a copy, see <https://opensource.org/licenses/MIT>.
//

/*************************************************************************//**
 * \file        logger.c
 * \brief       contains function for c-logger.
 * \author      Alexander Sacharov <as@asoft-labs.de> 
 *****************************************************************************/

//-----------------------------------------------------------------------------
// includes <...>
//-----------------------------------------------------------------------------
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <sys/stat.h>

//-----------------------------------------------------------------------------
// includes "..."
//-----------------------------------------------------------------------------
#include "iecpower/logging/logger.h"
#include "iecpower/logging/details/logserver.h"
#include "iecpower/logging/details/logsyslog.h"
#include "iecpower/threading/cmutex.h"

//----------------------------------------------------------------------------
// Defines and Macros
//----------------------------------------------------------------------------
#define MAXLINE            4096     /* max text line length */
#define CHARS_PER_LINE     32
#define CHARS_BLOCK        15

#define ADD_STRERR         true
#define NO_STRERR          false

#define ADD_LF             true
#define NO_LF              false

//#define DEBUG_LOGGER
#if defined( DEBUG_LOGGER )
#   define DBG( ... )	do { fprintf( stderr, __VA_ARGS__ ); } while (0)
#else
#   define DBG( ... )
#endif

#define STRNCAT( s1, s2, l, ov ) \
{ \
    if (strlen(s1) + strlen(s2) < ((unsigned)l - 10 )) { \
        ov = 0; \
        strcat( s1, s2 ); \
	}\
    else {\
        ov = 1; \
        strcpy( s1, "#OV" ); \
    } \
}

#define TESTMASK(p_,m_)  ( (!m_) || (LOG_PRI(p_) <= LOG_PRI( m_ )) )

//----------------------------------------------------------------------------
// Typedefs, enums, unions, variables
//----------------------------------------------------------------------------

/* logger context. For future use */
typedef struct logger_ctx_t {
   bool           created;
   bool           block;
   int            mask;              /* mask of priorities to be logged */

   bool           active_syslog;
   char           syslog_host[16];
   uint16_t       syslog_port;

   bool           active_listener;
   uint16_t       listen_port;

   bool           active_file;
   int            filed;
   uint32_t       filemaxsize;

   c_Semaphore  sem;
   c_Event      event;
} logger_ctx_t;

static logger_ctx_t  log_context;
static bool inited = false;

static void logger_va_print( int logLevel, bool prnStrError, bool add_nl, const char *fmt, va_list ap );

//----------------------------------------------------------------------------
// Functions Prototypes
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// Function Definitions
//----------------------------------------------------------------------------

/*************************************************************************//**
 * @brief   create & init.
 * @param[in] time mask.
 * @return logger context.
 */
bool logger_create( const char *fmt )
{
   if ( !inited ) {
      inited = true;
      log_context.created        = true;
      log_context.block          = false;
      log_context.mask           = 0;
      log_context.active_syslog  = false;
      log_context.active_listener= false;
      log_context.active_file    = false;

      log_context.syslog_host[0] = '\0';
      log_context.syslog_port    = 514;

      log_context.listen_port    = 0;
      log_context.filed          = -1;
      log_context.filemaxsize    = 2048000;  // 2 Mb

      c_CreateSem(log_context.sem);
      c_CreateEvent(log_context.event);

      // log server
      if ( logsrv_init() < 0 ) {
         perror( "logger_create: cannot init log server" );
      }

      if ( fmt != NULL ) {
         logsrv_set_timestamp_format( fmt );
      }
   }
   return true;
}

/*************************************************************************//**
 * destroy
 */
void logger_destroy( void )
{
   inited = false;
   log_context.created = false;

   if ( log_context.active_syslog ) {
      log_context.active_syslog = false;
      logclient_closelog();
   }

   if ( log_context.active_listener ) {
      log_context.active_listener = false;
      logsrv_socket_stop( log_context.listen_port );
   }

   if ( log_context.active_file ) {
      log_context.active_file = false;
      if ( log_context.filed != -1 ) {
         logsrv_fd_stop( log_context.filed );
         log_context.filed = -1;
      }
   }

   log_context.syslog_host[0] = '\0';
   log_context.syslog_port = 514;
   log_context.listen_port = 0;
   log_context.filed = -1;
   log_context.filemaxsize = 2048000;  // 2 Mb

   c_DeleteSem( log_context.sem );
   c_DeleteEvent( log_context.event );
}

/*************************************************************************//**
 * open syslog client.
 */
int logger_open_syslog( const char* logIp, uint16_t logPort, const char* ident, int options,
                        int logfac )
{

   if( logclient_openlog ( logIp, logPort, ident, options, logfac) == -1) {
      perror("logger cannot open logclient");
      return ( -1 );
   } else {
      log_context.active_syslog = true;
      log_context.syslog_port   = logPort;
      log_context.mask          = logfac;
      strncpy( log_context.syslog_host, logIp, sizeof log_context.syslog_host);
      return ( 0 );
   }
}

/*************************************************************************//**
 * close syslog output
 */
void logger_close_syslog( void )
{
   log_context.active_syslog = false;
   logclient_closelog();
}

/*************************************************************************//**
 * service global functions
 * @param mask 
 * @return 
 */
int logger_setlogmask( int mask )
{
   log_context.mask = mask;
   return logclient_setlogmask(mask);
}

void logger_closeall( void )
{
   logger_close_file();
   logger_close_socket();
   logger_close_syslog();
}

void logger_block( void )
{
   log_context.block = true;
}

void logger_unblock( void )
{
   log_context.block = false;
}

int logger_logstatus( void )
{
   return log_context.active_listener || log_context.active_syslog || log_context.active_file;
}
/*************************************************************************//**
 * open socket output
 * @param port 
 * @param level 
 * @param flags 
 * @return 
 */
int logger_open_socket( uint16_t port, int level, int flags )
{
   logger_close_socket();

   log_context.listen_port = port;

   if ( 0 != logsrv_socket_start( log_context.listen_port, level, flags ) ) {
      log_context.active_listener = false;
      return ( -1 );
   } else {
      log_context.active_listener = true;
      return ( 0 );
   }
}

/*************************************************************************//**
 * @brief close socket output
 */
void logger_close_socket( void )
{
   log_context.active_listener = false;
   logsrv_socket_stop( log_context.listen_port );
}

/*************************************************************************//**
 * open  file logging
 * @param fname 
 * @param level 
 * @param flags 
 * @return 
 */
int logger_open_file( const char *fname, int level, int flags )
{
   logger_close_file();

   log_context.filed = creat( fname, 0644 );
   if ( log_context.filed == -1 ) {
      log_context.filed = open( fname, O_WRONLY | O_APPEND );
   }

   if ( log_context.filed < 0 ) {
      log_context.active_file = false;
      perror( "can't open logfile" );
      return ( -1 );
   }
   log_context.active_file = true;
   logsrv_fd_start( log_context.filed, level, flags );
   return 0;
}

/*************************************************************************//**
 * cut off size of file
 * @param maxoff 
 */
void logger_trunc_file( off_t maxoff )
{
   if ( log_context.filed > 0 ) {
      struct stat stat;

      fstat( log_context.filed, &stat );
      if ( stat.st_size > maxoff ) {
         /* truncate */
         lseek( log_context.filed, 0, SEEK_SET );
         ftruncate( log_context.filed, maxoff );
      }
   }
}

/*************************************************************************//**
 * close stream to file
 */
void logger_close_file( void )
{
   if ( log_context.filed > 0 ) {
      logsrv_fd_stop( log_context.filed );
      log_context.filed = -1;
      log_context.active_file = false;
   }
}

/*************************************************************************//**
 * log with string error
 * @param priority 
 * @param addnl 
 * @param fmt 
 * @param ... 
 */
void logger_with_strerr( int priority, bool addnl, const char *fmt, ... )
{
   if ( TESTMASK(priority,log_context.mask) ) {
      va_list ap;
      va_start( ap, fmt );
      logger_va_print( priority, ADD_STRERR, addnl, fmt, ap );
      va_end( ap );
   }
}

void logger_print( int priority, bool addnl, const char* fmt, ... )
{
   if ( TESTMASK( priority, log_context.mask ) ) {
      va_list ap;
      va_start( ap, fmt );
      logger_va_print( priority, NO_STRERR, addnl, fmt, ap );
      va_end( ap );
   }
}

/*************************************************************************//**
 * Fatal error unrelated to a system call. Print a message and terminate.
 * @param priority 
 * @param addnl 
 * @param fmt 
 * @param ... 
 */
void logger_quit( int priority, bool addnl, const char* fmt, ... )
{
   va_list ap;
   va_start( ap, fmt );
   logger_va_print( priority, NO_STRERR, addnl, fmt, ap );
   va_end( ap );

   perror( "logger_quit exit");

   sleep( 1 );
   exit( 1 );
}

/*************************************************************************//**
 * buffer print
 * @param priority 
 * @param vpBuf 
 * @param length 
 */
void logger_hexdump( int priority, void* vpBuf, int length )
{
   if ( TESTMASK( priority, log_context.mask ) ) {
      char bufstr[512];
      char buf[MAXLINE];
      int i, j, s = 0, offset = 0;
      unsigned char *buffer;
      unsigned char *a;
      unsigned char overflow = 0;

      memset( buf, 0, MAXLINE );

      if ( vpBuf == NULL ) {
         return;
      }

      buffer = (unsigned char*) vpBuf;
      for ( i = 0; i < length; i += CHARS_PER_LINE ) {
         sprintf( bufstr, "(%04d)0x%04X: ", i + offset, i + offset );
         STRNCAT( buf, bufstr, MAXLINE, overflow );
         if ( overflow ) {
            // log and exit
            logger_print( priority, NO_LF, buf );
            break;
         };
         a = (buffer + i);
         for ( j = 0; j < CHARS_PER_LINE; j++ ) {
            if ( j == CHARS_BLOCK )
               sprintf( bufstr, "%02X  ", *a++ );
            else
               sprintf( bufstr, "%02X ", *a++ );
            STRNCAT( buf, bufstr, MAXLINE, overflow );
            if ( overflow ) {
               // log and exit
               logger_print( priority, NO_LF, buf );
               return;
            };
            s++;
            if ( s == length ) {
               STRNCAT( buf, "\n", MAXLINE, overflow );
               if ( overflow ) {
                  // log and exit
                  logger_print( priority, NO_LF, buf );
                  return;
               };
               // log and exit
               logger_print( priority, NO_LF, buf );
               return;
            }
         }
         STRNCAT( buf, "\n", MAXLINE, overflow );
         if ( overflow ) {
            // log and exit
            logger_print( priority, NO_LF, buf );
            break;
         };
         // log and clear
         logger_print( priority, NO_LF, buf );
         memset( buf, 0, sizeof(buf) );
      }
   }
}


/*************************************************************************//**
 * dumps size bytes of *data to stdout. Looks like:
 * [0000] 75 6E 6B 6E 6F 77 6E 20   30 FF 00 00 00 00 39 00 unknown 0.....9.
 * @param priority 
 * @param data 
 * @param size 
 */
void logger_hexascii_dump( int priority, void *data, int size )
{
   if ( TESTMASK( priority, log_context.mask ) ) {

      /* dumps size bytes of *data to stdout. Looks like:
       * [0000] 75 6E 6B 6E 6F 77 6E 20
       *                  30 FF 00 00 00 00 39 00 unknown 0.....9.
       * (in a single line of course)
       */
      unsigned char *p = data;
      unsigned char c;
      int n;
      char bytestr[4] = { 0 };
      char addrstr[10] = { 0 };
      char hexstr[16 * 3 + 5] = { 0 };
      char charstr[16 * 1 + 5] = { 0 };
      for ( n = 1; n <= size; n++ ) {
         if ( n % 16 == 1 ) {
            /* store address for this line */
            snprintf( addrstr, sizeof(addrstr), "%.4x", ((int) (size_t) p - (int) (size_t) data) );
         }

         c = *p;
         if ( c < 0x20 ) {
            c = '.';
         }

         /* store hex str (for left side) */
         snprintf( bytestr, sizeof(bytestr), "%02X ", *p );
         strncat( hexstr, bytestr, sizeof(hexstr) - strlen( hexstr ) - 1 );

         /* store char str (for right side) */
         snprintf( bytestr, sizeof(bytestr), "%c", c );
         strncat( charstr, bytestr, sizeof(charstr) - strlen( charstr ) - 1 );

         if ( n % 16 == 0 ) {
            /* line completed */
            logger_print( priority, NO_LF, "[%4.4s]   %-50.50s  %s\n", addrstr, hexstr, charstr );
            hexstr[0] = 0;
            charstr[0] = 0;
         } else if ( n % 8 == 0 ) {
            /* half line: add whitespaces */
            strncat( hexstr, "  ", sizeof(hexstr) - strlen( hexstr ) - 1 );
            strncat( charstr, " ", sizeof(charstr) - strlen( charstr ) - 1 );
         }
         p++; /* next byte */
      }

      if ( strlen( hexstr ) > 0 ) {
         /* print rest of buffer if not empty */
         logger_print( priority, NO_LF, "[%4.4s]   %-50.50s  %s\n", addrstr, hexstr, charstr );
      }
   }
}

/*************************************************************************//**
 * @brief   : Print a message and return to caller.
 * @note    : Caller specifies "errnoflag" and "level".
 * argumets :
 * return   :
 **/
static
#if defined(__GNUC__)
__attribute__ ((used))
#endif
void logger_va_print( int logLevel, bool prnStrError, bool add_nl, const char *fmt, va_list ap )
{
   if ( !log_context.block && log_context.active_syslog ) {
      // lock
      c_TakeSem(log_context.sem);

      if( !prnStrError ) {
         // easy log
         logclient_vsyslog (logLevel, fmt, ap);
      } else {
         char buf[MAXLINE];
         snprintf(buf, MAXLINE, "%s %m", fmt);
         logclient_vsyslog( logLevel, buf, ap );
      }
      // unlock
      c_ReleaseSem(log_context.sem);

   } else {
      int errno_save, n;
      char buf[MAXLINE];
      errno_save = errno; /* value caller might want printed */

      vsnprintf( buf, sizeof(buf), fmt, ap ); /* this is safe */

      n = strlen( buf );
      if ( prnStrError ) {
         snprintf( buf + n, sizeof(buf) - n, ": %s", strerror( errno_save ) );
      }

      if ( add_nl ) {
         strcat( buf, "\n" );
      }

      if ( !log_context.block && (log_context.active_listener || log_context.active_file) ) {
         strcat( buf, "\r" );
         logsrv_puts( 0, buf );
      } else {
         fputs( buf, stderr );
         fflush( stdout );
         fflush( stderr );
      }
   }
}

