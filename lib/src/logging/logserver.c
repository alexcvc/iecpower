//
// Copyright (c) 2011 Alexander Sacharov <a.sacharov@gmx.de>
//               All rights reserved.
//
// This work is licensed under the terms of the MIT license.
// For a copy, see <https://opensource.org/licenses/MIT>.
//

/*************************************************************************//**
 * \file        logserver.c
 * \brief       contains log-output to TCP connections (server in local host).
 *
 * \author      Alexander Sacharov <as@asoft-labs.de>
 * \date        2011-02-27
 *****************************************************************************/

//-----------------------------------------------------------------------------
// includes <...>
//-----------------------------------------------------------------------------
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <stdarg.h>
#include <time.h>
#include <signal.h>
#include <pthread.h>
#include <syslog.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/uio.h>

//-----------------------------------------------------------------------------
// includes "..."
//-----------------------------------------------------------------------------
#include "iecpower/logging/details/logserver.h"

//----------------------------------------------------------------------------
// Defines and Macros
//----------------------------------------------------------------------------
#if defined(DEBUG) && DEBUG == 1
#define DBG(...) do { fprintf(stderr, "%s: ", __func__); fprintf(stderr, __VA_ARGS__); } while (0)
#else
#define DBG(...)
#endif

#define BUFFER_LEN 1024

#define LOGGING_ST_DOWN     (1<<16)			/* channel is down */
#define LOGGING_ST_OVRFL    (1<<17)			/* overflow detected */

#define NEXT(s) ((typeof(s))((s)->link.next))
#define PREV(s) ((typeof(s))((s)->link.prev))

#define LC_LOCK()	pthread_mutex_lock(&log_channels_mutex)
#define LC_UNLOCK()	pthread_mutex_unlock(&log_channels_mutex)

#define LS_LOCK()	pthread_mutex_lock(&log_sockets_mutex)
#define LS_UNLOCK()	pthread_mutex_unlock(&log_sockets_mutex)

//----------------------------------------------------------------------------
// Typedefs, enums, unions, variables
//----------------------------------------------------------------------------
/**
 * @brief : structures
 **/

struct link {
   struct link *next, *prev;
};

struct log_channel {
   struct link link;

   int fd;
   int lvl;
   int flags;
   int state;

   void *priv;
};

struct log_socket {
   struct link link;

   int port;
   int fd;
   int lvl;
   int flags;

   pthread_t accepter;
};

static const char *fmt_timestamp = LOGGING_DEFAULT_FMT_TIMESTAMP;
static int default_lvl = 1;
static int max_lvl = -1;
static char str_timestamp[64];

static struct log_channel *log_channels = NULL;
static pthread_mutex_t log_channels_mutex = PTHREAD_MUTEX_INITIALIZER;

static struct log_socket *log_sockets = NULL;
static pthread_mutex_t log_sockets_mutex = PTHREAD_MUTEX_INITIALIZER;

static int syslog_active = 0;
static int *syslog_prio_map = NULL;
static int syslog_prio_map_len = 0;

static void *accepter( void *arg );

//----------------------------------------------------------------------------
// Function Definitions
//----------------------------------------------------------------------------

/**
 * @brief :
 **/
int logsrv_init( void ) {
   signal( SIGPIPE, SIG_IGN );
   return 0;
}

/*************************************************************************//**
 * set timestamp format (strftime() format)
 * @param fmt
 */
void logsrv_set_timestamp_format( const char *fmt ) {
   memset( str_timestamp, 0, sizeof(str_timestamp) );
   strncpy( str_timestamp, fmt, sizeof(str_timestamp) );
   fmt_timestamp = &str_timestamp[0];
}

/*************************************************************************//**
 * set default logging
 * @param lvl
 * @return <0> if successfully, otherwise - <-1>
 */
int logsrv_set_default_level( int lvl ) {
   int old_lvl = default_lvl;

   if ( lvl >= 0 ) {
      default_lvl = lvl;
   }

   return old_lvl;
}

/*************************************************************************//**
 * find fd
 * @param fd
 * @return channel or null
 */
static struct log_channel *find_fd( int fd ) {
   struct log_channel *ch;

   for ( ch = log_channels; ch; ch = NEXT( ch ) )
      if ( ch->fd == fd ) {
         return ch;
      }

   return NULL;
}

/*************************************************************************//**
 * determinate max level
 */
static void determine_max_lvl( void ) {
   struct log_channel *ch;

   max_lvl = -1;

   for ( ch = log_channels; ch; ch = NEXT( ch ) )
      if ( ch->lvl > max_lvl ) {
         max_lvl = ch->lvl;
      }
}

/*************************************************************************//**
 * add to list
 * @param vhead
 * @param elem
 */
static void add_to_list( void *vhead, struct link *elem ) {
   struct link **head = vhead;
   elem->prev = NULL;
   elem->next = *head;

   if ( *head ) {
      (*head)->prev = elem;
   }

   *head = elem;
}

/*************************************************************************//**
 * remove link from list
 * @param vhead
 * @param elem
 */
static void remove_from_list( void *vhead, struct link *elem ) {
   struct link **head = vhead;
   if ( elem->next ) {
      elem->next->prev = elem->prev;
   }
   if ( elem->prev ) {
      elem->prev->next = elem->next;
   }

   if ( *head == elem ) {
      *head = elem->next;
   }
}

/*************************************************************************//**
 * add channel
 * @param ch
 */
static void add_log_channel( struct log_channel *ch ) {
   add_to_list( &log_channels, &ch->link );
   if ( ch->lvl > max_lvl ) {
      max_lvl = ch->lvl;
   }
}

/*************************************************************************//**
 * remove channel
 * @param ch
 */
static void remove_log_channel( struct log_channel *ch ) {
   remove_from_list( &log_channels, &ch->link );
   determine_max_lvl();
}

/*************************************************************************//**
 * close and down channel
 */
static void close_down_channels( void ) {
   struct log_channel *ch = log_channels;

   while ( ch ) {
      LC_LOCK();

      for ( ch = log_channels; ch; ch = NEXT( ch ) ) {
         if ( ch->state & LOGGING_ST_DOWN ) {
            int fd = ch->fd;
            LC_UNLOCK();
            logsrv_fd_stop( fd );
            break;
         }
      }

      if ( !ch ) {
         LC_UNLOCK();
      }
   }
}

/*************************************************************************//**
 * start fd
 * @param fd
 * @param lvl
 * @param flags
 * @param priv
 * @return <0> if successfully, otherwise - <-1>
 */
static int logsrv_fd_start_priv( int fd, int lvl, int flags, void *priv ) {
   struct log_channel *ch;

   LC_LOCK();

   ch = find_fd( fd );
   if ( ch ) {
      ch->lvl = lvl;
      ch->flags = flags;
      LC_UNLOCK();
      return 0;
   }

   ch = malloc( sizeof(*ch) );
   if ( !ch ) {
      LC_UNLOCK();
      DBG("Error: out of memory\n");
      return -1;
   }

   ch->fd = fd;
   ch->lvl = lvl;
   ch->flags = flags;
   ch->state = 0;
   ch->priv = priv;

   fcntl( fd, F_SETFL, fcntl( fd, F_GETFL ) | O_NONBLOCK );
   add_log_channel( ch );

   LC_UNLOCK();

   DBG("Started fd %d, priv = %p\n", fd, priv);
   return 0;
}

/*************************************************************************//**
 * start log to file descriptor
 * @param fd
 * @param lvl
 * @param flags
 * @return
 */
int logsrv_fd_start( int fd, int lvl, int flags ) {
   return logsrv_fd_start_priv( fd, lvl, flags, NULL );
}

/*************************************************************************//**
 * close file descriptor
 * @param fd
 * @return
 */
int logsrv_fd_stop( int fd ) {
   struct log_channel *ch;

   DBG("Stopping fd %d\n", fd);

   LC_LOCK();
   ch = find_fd( fd );
   if ( !ch ) {
      LC_UNLOCK();
      return -1;
   }

   remove_log_channel( ch );
   LC_UNLOCK();

   if ( ch->flags & LOGGING_FL_CLOSE ) {
      DBG("Closing fd %d\n", ch->fd);
      close( ch->fd );
   }

   return 0;
}

/*************************************************************************//**
 * find socket
 * @param port
 * @return <0> if successfully, otherwise - <-1>
 */
static struct log_socket *find_socket( int port ) {
   struct log_socket *so;

   for ( so = log_sockets; so; so = NEXT( so ) )
      if ( so->port == port ) {
         break;
      }

   return so;
}

/*************************************************************************//**
 * prepare socket
 * @param port
 * @return
 */
static int prepare_socket( int port ) {
   struct sockaddr_in sock;
   int one = 1;
   int fd = socket( AF_INET, SOCK_STREAM, 0 );
   if ( fd < 0 ) {
      DBG("Error: can't create socket\n");
      return -1;
   }

   setsockopt( fd, SOL_SOCKET, SO_REUSEADDR, (char *) &one, sizeof(one) );

   memset( &sock, 0, sizeof(sock) );
   sock.sin_port = htons( port );

   sock.sin_family = AF_INET;
   sock.sin_addr.s_addr = htonl( INADDR_ANY );

   if ( bind( fd, (struct sockaddr *) &sock, sizeof(sock) ) < 0 ) {
      close( fd );
      DBG("Error: can't bind socket\n");
      return -1;
   }

   if ( listen( fd, 5 ) < 0 ) {
      close( fd );
      DBG("Error: can't listen on socket\n");
      return -1;
   }

   return fd;
}

/*************************************************************************//**
 * start socket
 * @param port
 * @param lvl
 * @param flags
 * @return <0> if successfully, otherwise - <-1>
 */
int logsrv_socket_start( int port, int lvl, int flags ) {
   struct log_socket *so;
   int fd;
   pthread_attr_t pattr;

   pthread_attr_init( &pattr );

   LS_LOCK();
   so = find_socket( port );
   if ( so ) {
      so->lvl = lvl;
      so->flags = flags;
      LS_UNLOCK();
      return 0;
   }

   so = malloc( sizeof(*so) );
   if ( !so ) {
      LS_UNLOCK();
      DBG("Error: out of memory\n");
      return -1;
   }

   fd = prepare_socket( port );
   if ( fd < 0 ) {
      LS_UNLOCK();
      return -1;
   }

   so->port = port;
   so->fd = fd;
   so->lvl = lvl;
   so->flags = flags;

   if ( pthread_create( &so->accepter, &pattr, accepter, so ) < 0 ) {
      LS_UNLOCK();
      close( fd );
      pthread_attr_destroy( &pattr );
      DBG("Error: can't create thread\n");
      return -1;
   }

   add_to_list( &log_sockets, &so->link );

   LS_UNLOCK();

   pthread_attr_destroy( &pattr );
   return 0;
}

/*************************************************************************//**
 * down log to socket
 * @param lsock
 */
static void down_log_socket( struct log_socket *lsock ) {
   struct log_channel *ch;

   LC_LOCK();

   for ( ch = log_channels; ch; ch = NEXT( ch ) )
      if ( ch->priv == lsock ) {
         ch->state |= LOGGING_ST_DOWN;
      }

   LC_UNLOCK();

   close_down_channels();
}

/*************************************************************************//**
 * stop socket
 * @param port
 * @return
 */
int logsrv_socket_stop( int port ) {
   struct log_socket *so;

   LS_LOCK();
   so = find_socket( port );
   if ( !so ) {
      LS_UNLOCK();
      return -1;
   }

   close( so->fd );

   pthread_cancel( so->accepter );
   pthread_join( so->accepter, NULL );

   down_log_socket( so );
   remove_from_list( &log_sockets, &so->link );

   free( so );
   LS_UNLOCK();

   close_down_channels();
   return 0;
}

/*************************************************************************//**
 * start log to syslog
 * @param ident
 * @param option
 * @param facility
 * @return
 */
int logsrv_syslog_start( const char *ident, int option, int facility) {
   if ( syslog_active ) {
      closelog();
   }

   openlog( ident, option, facility );
   syslog_active = 1;

   return 0;
}

/*************************************************************************//**
 * stop log to socket
 * @return
 */
int logsrv_syslog_stop( void ) {
   if ( syslog_active ) {
      closelog();
      syslog_active = 0;
   }

   return 0;
}

/*************************************************************************//**
 * set prio mask
 * @param map
 * @param len
 */
void logsrv_syslog_set_prio_map( int *map, int len ) {
   if ( syslog_prio_map ) {
      free( syslog_prio_map );
   }

   syslog_prio_map = NULL;
   syslog_prio_map_len = 0;

   if ( !map ) {
      return;
   }

   syslog_prio_map = malloc( sizeof(syslog_prio_map[0]) * len );
   if ( !syslog_prio_map ) {
      return;
   }

   memcpy( syslog_prio_map, map, sizeof(syslog_prio_map[0]) * len );
   syslog_prio_map_len = len;
}

/*************************************************************************//**
 * fixed level
 * @param lvl
 */
static void fix_level( int *lvl ) {
   if ( *lvl < 0 ) {
      *lvl = default_lvl;
   }
}

/*************************************************************************//**
 * log print
 * @param lvl
 * @param fmt
 * @param ...
 * @return <0> if successfully, otherwise - <-1>
 */
int logsrv_printf( int lvl, const char *fmt, ... ) {
   va_list ap;
   char buffer[BUFFER_LEN];

   if ( !syslog_active ) {
      fix_level( &lvl );
      if ( lvl > max_lvl ) {
         return 0;
      }
   }

   va_start( ap, fmt );
   vsnprintf( buffer, sizeof(buffer), fmt, ap );
   va_end( ap );

   return logsrv_puts( lvl, buffer );
}

#ifndef BETWEEN
#define BETWEEN(a, x, b) ((x) < (a) ? (x) : ((x) > (b) ?  (b) : (x)))
#endif

/*************************************************************************//**
 * map syslog prio
 * @param lvl
 * @return <0> if successfully, otherwise - <-1>
 */
static int map_syslog_prio( int lvl ) {
   int prio;

   if ( syslog_prio_map ) {
      lvl = BETWEEN( 0, lvl, syslog_prio_map_len - 1 );
      prio = syslog_prio_map[lvl];
   } else {
      prio = lvl;
   }

   return BETWEEN( LOG_EMERG, prio, LOG_DEBUG );
}

/*************************************************************************//**
 * put message
 * @param lvl
 * @param buffer
 * @return <0> if successfully, otherwise - <-1>
 */
int logsrv_puts( int lvl, const char *buffer ) {
   struct iovec iov[2];
   struct log_channel *ch;
   time_t tim;
   struct tm tm;
   char tbuffer[64];
   int do_close_down = 0;

   fix_level( &lvl );

   if ( syslog_active ) {
      int prio = map_syslog_prio( lvl );
      if ( prio >= 0 ) {
         syslog( prio, "%s", buffer );
      }
   }

   if ( lvl > max_lvl ) {
      return 0;
   }

   tim = time( NULL );
   localtime_r( &tim, &tm );
   strftime( tbuffer, sizeof(tbuffer), fmt_timestamp, &tm );

   iov[0].iov_base = tbuffer;
   iov[0].iov_len = strlen( tbuffer );
   iov[1].iov_base = (char *) buffer;
   iov[1].iov_len = strlen( buffer );

   LC_LOCK();

   for ( ch = log_channels; ch; ch = NEXT( ch ) ) {
      int ret = 0;

      if ( lvl > ch->lvl ) {
         continue;
      }

      if ( ch->flags & LOGGING_FL_NOTIME ) {
         ret = write( ch->fd, buffer, iov[1].iov_len );
      } else {
         ret = writev( ch->fd, iov, 2 );
      }

      if ( ret < 0 ) {
         DBG("Error: write on %d: %d\n", ch->fd, errno);

         if ( errno == EWOULDBLOCK ) {
            ch->state |= LOGGING_ST_OVRFL;
         } else {
            do_close_down = 1;
            ch->state |= LOGGING_ST_DOWN;
         }
      }
   }

   LC_UNLOCK();

   if ( do_close_down ) {
      close_down_channels();
   }

   return 0;
}

/*************************************************************************//**
 * acceptor messages
 * @param arg
 * @return NULL
 */
static void *accepter( void *arg ) {
   struct log_socket *lsock = (struct log_socket *) arg;
   struct sockaddr sockaddr;

   while ( 1 ) {
      socklen_t addrlen = sizeof(sockaddr);
      DBG("Accepting on %d\n", lsock->fd);
      int fd = accept( lsock->fd, &sockaddr, &addrlen );
      DBG("Accepted %d\n", fd);
      if ( fd < 0 ) {
         break;
      }

      logsrv_fd_start_priv( fd, lsock->lvl, lsock->flags | LOGGING_FL_CLOSE, lsock );
   }

   return NULL;
}
