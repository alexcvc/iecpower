# IECPower

***This project is currently under migration from previous SVN repositories to initial release under git.***

The IEC power automation library contains a set of C++ classes that can be used to create systems in the field for all IEC60870-5-x, 
IEC61850, DNP3, Modbus protocols in power automation and data communication. 

## History

* 2009 - first revision of the C++ class library;
* 2012 - new classes for IEC61850 quality and timestamps in C++11;
* 2013 - dynamic memory allocation to minimize mapping of mappings in embedded applications  
* 2014 - revision and new classes for DNP3; 
* 2015 - code migration from C to C++11;
* 2017 - support for MODBUS swapping as ModbusPoll;
* 2018 - revision of classes using C++11/14;
* 2020 - revision of all classes for IEC61850 ED2-1, IEC60870-5-101/103/104, DNP3 using C++11 and safe pointers.
* 2021 - code migration from SVN to GitHub;

## Requirements

* a C++11/14 compiler (`gcc-8.0` or above)
* [`cmake`](https://cmake.org) 3.10+
* [`conan`](https://conan.io) 1.28+
* [`spdlog`](https://github.com/gabime/spdlog/wiki/1.-QuickStart) 1.8+
* [`fmt`](https://fmt.dev/latest/index.html) 7.0+
* [`pugyxml`](https://pugixml.org/docs/manual.html) 1.10+

## Installation

You can find more information in doc directory.

#### Note

Code migration from GitLab to GitHub is in progress..
