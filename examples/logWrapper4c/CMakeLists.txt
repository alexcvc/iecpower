
add_executable(logWriterExample example.cpp)
target_link_libraries(logWriterExample iecpower ${CONAN_LIBS_SPDLOG} ${CMAKE_THREAD_LIBS_INIT})

add_executable(logWriterBenchmark bench.cpp)
target_link_libraries(logWriterBenchmark iecpower ${CONAN_LIBS_SPDLOG} ${CMAKE_THREAD_LIBS_INIT})

add_executable(logWriterMultisink multisink.cpp)
target_link_libraries(logWriterMultisink iecpower ${CONAN_LIBS_SPDLOG} ${CMAKE_THREAD_LIBS_INIT})

enable_testing()
file(MAKE_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}/logs")

add_test(NAME RunLogWriterExample COMMAND logWriterExample)
add_test(NAME RunLogWriterBenchmark COMMAND logWriterBenchmark)