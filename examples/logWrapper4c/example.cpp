//
// Copyright (c) 2021 Alexander Sacharov <a.sacharov@gmx.de>
//               All rights reserved.
//
// This work is licensed under the terms of the MIT license.
// For a copy, see <https://opensource.org/licenses/MIT>.
//

#include <iecpower/logging/logWriter.hh>

void syslog_example();
void user_defined_example();
void err_handler_example();

using namespace iecpower;
using iecpower::log::writer;

int main(int, char *[]) {

   try {
      log::writer logWriter("console");

      // Console logger with color
      logWriter.open_stdout_log(spdlog::level::debug);
      logWriter.info("Welcome to spdlog!");
      logWriter.error("Some error message with arg{}..", 1);

      // Conditional logging example
      logWriter.info_if(true, "Welcome to spdlog conditional logging!");

      // Formatting examples
      logWriter.warn("Easy padding in numbers like {:08d}", 12);
      logWriter.critical("Support for int: {0:d};  hex: {0:x};  oct: {0:o}; bin: {0:b}", 42);
      logWriter.info("Support for floats {:03.2f}", 1.23456);
      logWriter.info("Positional args are {1} {0}..", "too", "supported");
      logWriter.info("{:<30}", "left aligned");

      logWriter.drop_all();

      // Create a file rotating logger with 5mb size max and 3 rotated files
      logWriter.open_rotation_file_log("mylogfile.txt", 1024 * 5, 3);
      for (int i = 0; i < 100; ++i) {
         logWriter.info("{} * {} equals {:>10}", i, i, i * i);
      }

      // Customize msg format for all messages
      logWriter.info("This is another message with custom format");

      logWriter.drop_all();

//      // Runtime log levels
//      spdlog::set_level(spdlog::level::info); //Set global log level to info
//      logWriter.debug("This message shold not be displayed!");
//      logWriter.set_level(spdlog::level::debug); // Set specific logger's log level
//      logWriter.debug("This message shold be displayed..");
//
//      // Compile time log levels
//      // define SPDLOG_DEBUG_ON or SPDLOG_TRACE_ON
//      SPDLOG_TRACE(console, "Enabled only #ifdef SPDLOG_TRACE_ON..{} ,{}", 1, 3.23);
//      SPDLOG_DEBUG(console, "Enabled only #ifdef SPDLOG_DEBUG_ON.. {} ,{}", 1, 3.23);
//      SPDLOG_DEBUG_IF(console, true, "This is a debug log");
//
//      // syslog example. linux/osx only
//      syslog_example();
//
//      // android example. compile with NDK
//      android_example();
//
//      // Log user-defined types example
//      user_defined_example();
//
//      // Change default log error handler
//      err_handler_example();
//
//      // Apply a function on all registered loggers
//      spdlog::apply_all([&](std::shared_ptr<spdlog::logger> l) {
//         l->info("End of example.");
//      });
//
//      // Release and close all loggers
//      spdlog::drop_all();
   }
   catch (const spdlog::spdlog_ex &ex) {
      std::cout << "Log init failed: " << ex.what() << std::endl;
      return 1;
   }
}

//syslog example (linux/osx/freebsd)
void syslog_example() {
   std::string ident = "spdlog-example";
   auto syslog_logger = spdlog::syslog_logger_mt("syslog", ident, LOG_PID);
   syslog_logger->warn("This is warning that will end up in syslog.");
}

//
//custom error handler
//
void err_handler_example() {
   //can be set globaly or per logger(logger->set_error_handler(..))
   spdlog::set_error_handler([](const std::string &msg) {
      std::cerr << "my err handler: " << msg << std::endl;
   });
   spdlog::get("console")->info("some invalid message to trigger an error {}{}{}{}", 3);
}
