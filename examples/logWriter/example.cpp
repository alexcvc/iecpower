//
// Copyright (c) 2021 Alexander Sacharov <a.sacharov@gmx.de>
//               All rights reserved.
//
// This work is licensed under the terms of the MIT license.
// For a copy, see <https://opensource.org/licenses/MIT>.
//

/*************************************************************************//**
 * \file        example.cpp
 * \brief       contains example for spdlog wrapper
 * \author      Alexander Sacharov <as@asoft-labs.de>
 * \date        2021-07-11
 *****************************************************************************/

//-----------------------------------------------------------------------------
// includes <...>
//-----------------------------------------------------------------------------
#include <iostream>
#include <thread>
#include <vector>
#include <chrono>

//-----------------------------------------------------------------------------
// includes "..."
//-----------------------------------------------------------------------------
#include "iecpower/logging/logWrapper4c.h"

//----------------------------------------------------------------------------
// Defines and Macros
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// Typedefs, enums, unions, variables
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// Functions Prototypes
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// Function Definitions
//----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
int func_err_code(int errcode)
{
    return errcode;
}

//-----------------------------------------------------------------------------
int test_if_error()
{
    for (int i = 0; i < 2; ++i)
    {
       LOG4C_IF_ERROR_BREAK("console", i, "test_if_break");
    }

    for (int i = 0; i < 2; ++i)
    {
       LOG4C_IF_ERROR_CONTINUE("console", i, "test_if_continue");
    }

    for (int i = 0; i < 2; ++i)
    {
       LOG4C_IF_ERROR_GO_ON("console", i, "test_if_error_go_on");
    }

    for (int i = 0; i < 2; ++i)
    {
       LOG4C_IF_ERROR_RETURN("console", i, "test_if_error_return");
    }

    return 0;
}

//-----------------------------------------------------------------------------
int test_if_method_error()
{
    int e = 0;

    for (int i = 0; i < 2; ++i)
    {
       LOG4C_IF_METHOD_ERROR_BREAK("console", e, func_err_code(i));
    }

    for (int i = 0; i < 2; ++i)
    {
       LOG4C_IF_METHOD_ERROR_CONTINUE("console", e, func_err_code(i));
    }

    for (int i = 0; i < 2; ++i)
    {
        IF_METHOD_ERROR_GO_ON("console", e, func_err_code(i));
    }

    for (int i = 0; i < 2; ++i)
    {
       LOG4C_IF_METHOD_ERROR_RETURN("console", e, func_err_code(i));
    }

    return 0;
}

//-----------------------------------------------------------------------------
void worker()
{
    TRACE_FUNCTION("console");

    for (int i = 0; i < 2; i++)
    {
        LOG4C_LOG_DEBUG("console", "thread running");
        LOG4C_LOG_INFO("console", "thread running");
        LOG4C_LOG_WARN("console", "thread running");
        LOG4C_LOG_ERROR("console", i, "thread running");
        LOG4C_LOG_CRITI("console", i, "thread running");

        LOG4C_LOG_DEBUG_VAR("console", "thread running: %d", i);
        LOG4C_LOG_INFO_VAR("console", "thread running: %d", i);
        LOG4C_LOG_WARN_VAR("console", "thread running: %d", i);
        LOG4C_LOG_ERROR_VAR("console", i, "thread running: %d", i);
        LOG4C_LOG_CRITI_VAR("console", i, "thread running: %d", i);
    }
}

//-----------------------------------------------------------------------------
void worker_base_file()
{
    TRACE_FUNCTION("base_file");

    for (int i = 0; i < 2; i++)
    {
        LOG4C_LOG_DEBUG("base_file", "thread running");
        LOG4C_LOG_INFO("base_file", "thread running");
        LOG4C_LOG_WARN("base_file", "thread running");
        LOG4C_LOG_ERROR("base_file", i, "thread running");
        LOG4C_LOG_CRITI("base_file", i, "thread running");

        LOG4C_LOG_DEBUG_VAR("base_file", "thread running: %d", i);
        LOG4C_LOG_INFO_VAR("base_file", "thread running: %d", i);
        LOG4C_LOG_WARN_VAR("base_file", "thread running: %d", i);
        LOG4C_LOG_ERROR_VAR("base_file", i, "thread running: %d", i);
        LOG4C_LOG_CRITI_VAR("base_file", i, "thread running: %d", i);
    }
}

//-----------------------------------------------------------------------------
void worker_rout_file()
{
    TRACE_FUNCTION("rout_file");

    for (int i = 0; i < 2; i++)
    {
        LOG4C_LOG_DEBUG("rout_file", "thread running");
        LOG4C_LOG_INFO("rout_file", "thread running");
        LOG4C_LOG_WARN("rout_file", "thread running");
        LOG4C_LOG_ERROR("rout_file", i, "thread running");
        LOG4C_LOG_CRITI("rout_file", i, "thread running");

        LOG4C_LOG_DEBUG_VAR("rout_file", "thread running: %d", i);
        LOG4C_LOG_INFO_VAR("rout_file", "thread running: %d", i);
        LOG4C_LOG_WARN_VAR("rout_file", "thread running: %d", i);
        LOG4C_LOG_ERROR_VAR("rout_file", i, "thread running: %d", i);
        LOG4C_LOG_CRITI_VAR("rout_file", i, "thread running: %d", i);
    }
}

/**
 * main
 * @param argc
 * @param argv
 * @return <0> if successfully, otherwise - <-1>
 */
int main()
{
   {
      log4c_set_level(LOG4C_TRACE);
      log4c_set_pattern("[%Y-%m-%d %H:%M:%S.%e][%L][%t] %v");

      log4c_stdout_color_mt("console");

      LOG4C_LOG_INFO("console", "main thread start");

      std::vector<std::thread> vecThread;
      for (int i = 0; i < 2; ++i) {
         vecThread.emplace_back(worker);
      }

      for (size_t i = 0; i < vecThread.size(); ++i) {
         vecThread[i].join();
      }

      test_if_error();
      test_if_method_error();

      LOG4C_LOG_INFO("console", "main thread end");

      log4c_drop_all();
   }

   {
      log4c_set_level(LOG4C_TRACE);
      log4c_set_pattern("[%Y-%m-%d %H:%M:%S.%e][%L][%t] %v");

      log4c_basic_logger_mt("base_file", "base_file.txt", true);

      LOG4C_LOG_INFO("base_file", "base_file thread start");

      std::vector<std::thread> vecThread;
      for (int i = 0; i < 2; ++i) {
         vecThread.emplace_back(worker_base_file);
      }

      for (size_t i = 0; i < vecThread.size(); ++i) {
         vecThread[i].join();
      }

      LOG4C_LOG_INFO("base_file", "base_file thread end");

      log4c_drop_all();
   }

   {
      log4c_set_level(LOG4C_TRACE);
      log4c_set_pattern("[%Y-%m-%d %H:%M:%S.%e][%L][%t] %v");

      log4c_rotating_logger_mt("worker_rout_file", "rout_file.txt", 1024 * 2, 5);

      LOG4C_LOG_INFO("worker_rout_file", "worker_rout_file thread start");

      std::vector<std::thread> vecThread;
      for (int i = 0; i < 2; ++i) {
         vecThread.emplace_back(worker_rout_file);
      }

      for (size_t i = 0; i < vecThread.size(); ++i) {
         vecThread[i].join();
      }

      LOG4C_LOG_INFO("worker_rout_file", "worker_rout_file thread end");

      log4c_drop_all();
   }

   return 0;
}
