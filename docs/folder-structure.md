
Folder structure and naming conventions
=======================================

## Naming conventions

Use *short lowercase names* at least for the *top-level files* and folders except `LICENSE`, `README.md`

### A root directory

```text
 .
 |- conanfile.txt                # the main `conan` configuration file listing dependencies
 |- CMakeLists.txt               # the main `CMake` Project configuration file
 |- .idea/                       # CLion project files (optional)
 |- .conan                       # files how to access (or create) a development container
     |- conansetup.sh            # conan setup file 
 |- .gitignore                   # files to be excluded by git
 |-- docs/                       # Documentation files
 |-- cmake/                      # CMake modules and files
 |-  lib/                        # the whole C/C++ project
     |- CMakeLists.txt           # the `CMake` configuration file
     |- src/                     # library source files
     |- include/iecpower         # library header files
 |-- examples/                   # none automated tests and related samples
 |-- tests/                      # tests (optional for any extra tools or files)
 |-- submodules/                 # submodules
 |-- CONTRIBUTING.md             # contributing rules
 |-- LICENSE                     # licence
 |-- README.md                   # this readme
```

